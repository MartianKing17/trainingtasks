#include <cstring>
#include <cctype>
#include "string2.h"
using std::cin;
using std::cout;

int String::num_strings = 0;

int String::howMany()
{
    return num_strings;
}

String::String(const char * s)
{
    len = std::strlen(s);
    str = new char[len + 1];
    std::strcpy(str, s);
    num_strings++;   
}

String::String()
{
    len = 4;
    str = new char[1];
    str[0] = '\0';
    num_strings++;
}

String::String(const String &st)
{
    num_strings++;
    len = st.len;
    str = new char [len + 1];
    std::strcpy(str, st.str);
}

String::~String()
{
    --num_strings;
    delete[] str;
}

void String::stringlow()
{
    for(int i = 0; i < len; ++i) {
        str[i] = tolower(str[i]);
    }
}

void String::stringup()
{
    for(int i = 0; i < len; ++i) {
        str[i] = toupper(str[i]);
    }
}

int String::has(char c)
{
    int num{};
    for(int i = 0; i < len; ++i) {
        if (str[i] == c) {
            ++num;
        }
    }
    return num;    
}

String &String::operator=(const String &st)
{
    if (this == &st)
        return *this;
    delete[] str;
    len = st.len;
    str = new char[len + 1];
    std::strcpy(str, st.str);
    return *this;
}

String &String::operator=(const char * s)
{
    delete[] str;
    len = std::strlen(s);
    str = new char[len + 1];
    std::strcpy(str, s);
    return *this;
}

char &String::operator[](int i)
{
    return str[i];
}

const char &String::operator[](int i) const
{
    return str[i];
}

bool operator<(const String &s1, const String s2)
{
    return std::strcmp(s1.str, s2.str) < 0;
}

bool operator>(const String &s1, const String s2)
{
    return s2.str < s1.str;
}

bool operator==(const String &s1, const String s2)
{
    return (strcmp(s1.str, s2.str) == 0);
}

ostream &operator<<(ostream &os, const String s)
{
    os << s.str;
    return os;
}

istream &operator>>(istream &is, String &s)
{
    delete[] s.str;
    s.len = 30;
    s.str = new char[s.len + 1]{};
    is.getline(s.str, s.len);
    return is;
}

String &String::operator+(const String &s)
{
    int oldLen = len;
    len += s.len;
    char * newStr = new char[len + 1];
    std::strcpy(newStr, str);
    delete[] str;
    str = newStr;
    newStr = nullptr;    

    for (int i = 0, j = oldLen; i < s.len; ++i, ++j) {
        str[j] = s.str[i]; 
    }
       
    return *this;
}

String operator+(const char * s1, const String s2)
{
    int len = std::strlen(s1) + s2.len;
    char * str = new char[len + 1];
    std::strcpy(str, s1);
    
    for (int i = 0, j = std::strlen(s1); i < s2.len; ++i, ++j) {
        str[j] = s2.str[i]; 
    }

    String res(str);
    delete[] str;
    return res;
}

String operator+(const String s1, const char *s2)
{
    return s2 + s1;
}

