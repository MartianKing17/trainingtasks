#pragma once
#include <iostream>
using std::ostream;
using std::istream;

class String
{
public:
    String(const char * s);
    String();
    String(const String &s);
    ~String();
    int lenght() const {return len;}
    String &operator=(const String &s);
    String &operator=(const char *s);
    char &operator[](int i);
    const char &operator[](int i) const;
    String &operator+(const String &s);
    void stringlow();
    void stringup();
    int has(char c);
    friend bool operator<(const String &s1, const String s2);
    friend bool operator>(const String &s1, const String s2);
    friend bool operator==(const String &s1, const String s2);
    friend String operator+(const char * s1, const String s2);
    friend String operator+(const String s1, const char *s2);
    friend ostream &operator<<(ostream &os, const String s2);
    friend istream &operator>>(istream &is, String &s2);
    static int howMany();
private:
    char * str;
    int len;
    static int num_strings;
    static const int CINLIM = 80;
};
