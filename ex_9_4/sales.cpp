#include <iostream>
#include <algorithm> 
#include <numeric>
#include "sales.h"

using std::cin;
using std::cout;
using std::endl;


namespace sales
{
    void setSales(Sales & s, const double arr[], int n) 
	{
		for (int i = 0; i < n ; ++i) {
				s.sales[i] = arr[i];
		}
		s.averange = std::accumulate(s.sales, s.sales + quarters, 0.0) / quarters;
		s.max = *std::max_element(s.sales, s.sales + quarters);
		s.min = *std::min_element(s.sales, s.sales + quarters);
	}

    void setSales(Sales & s)
	{
		int sales{};
		for (int i = 0; i < quarters; ++i) {
        		cout << "Enter how many sales in #" << (i + 1) << " quarter:\n" ;
        		for (int j = 0; j < 3; ++j) {
				cout << "The " << (j + 1) << " month: ";
				cin >> sales;
				s.sales[i] += sales;
			}
		}

		s.averange = std::accumulate(s.sales, s.sales + quarters, 0.0) / quarters;
		s.max = *std::max_element(s.sales, s.sales + quarters);
		s.min = *std::min_element(s.sales, s.sales + quarters);
	}

    void showSales(const Sales & s)
	{
		for (int i = 0; i < quarters; ++i) {
			cout << "In the quarter #" << (i + 1) << ": " << s.sales[i] << '\n';
		}

		cout << "Averange: " << s.averange;
		cout << "\nMax: "    << s.max;
		cout << "\nMin: "    << s.min << endl;
	}
}
