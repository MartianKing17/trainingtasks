#include <iostream>
#include "sales.h"

using namespace std;
using namespace sales;

int main()
{
    Sales s1{}, s2{};
    double arr[quarters];
    setSales(s1);
    
    for(int i = 0; i < quarters; ++i) {
        cout << "Enter how many sales in #" << (i + 1) << " quarter: " ;
        cin >> arr[i];
    }

    setSales(s2, arr, quarters);
    cout << "Information about first 4 quarters:\n";
    showSales(s1);
    cout << "Information about second 4 quarters:\n";
    showSales(s2);
}
