
namespace sales
{
    const int quarters = 4;
    struct Sales
    {
        double sales[quarters];
        double averange;
        double max;
        double min;
    };

    void setSales(Sales & s, const double arr[], int n);
    void setSales(Sales & s);
    void showSales(const Sales & s);
}
