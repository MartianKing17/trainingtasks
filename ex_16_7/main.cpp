#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <cstdlib>

using namespace std;

vector<int> Lotto(int range, int numOfRandomNumbers);

int main()
{
    srand(time(0));
    int range{}, numOfRandomNumbers = 6;
    vector<int> winners;
    cout << "Enter number of numbers in the lottety card: ";
    cin  >> range;
    winners = Lotto(range, numOfRandomNumbers);
    cout << "The winner number: ";
    for(const int x : winners) {
        cout << x << ' ';
    }
    cout << endl;
    return 0;
}

vector<int> Lotto(int range, int numOfRandomNumbers)
{
    vector<int> vec(range);
    iota(vec.begin(), vec.end(), 1);
    random_shuffle(vec.begin(), vec.end());
    vec.erase(vec.begin() + numOfRandomNumbers, vec.end());
    return vec;
}

