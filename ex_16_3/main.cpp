#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <cctype>
#include <stdexcept>

using namespace std;

void readFile(vector<string> &wordlist)
{
    string data{};
    ifstream readFile("test_doc.txt");

    if (!readFile.is_open()) {
        throw runtime_error("Could not open file");
    }    

    while(!readFile.eof()) {
        readFile >> data;
        wordlist.push_back(data);
    }
}

int main()
{
    srand(time(0));
    vector<string> wordlist;
    char play{}, letter{};
    string target{}, badchars{}, attempt{};
    int length{}, guesses{}, loc{};

    try {
        readFile(wordlist);
    } catch (runtime_error &e) {
        cout << e.what() << endl;
        return 0;
    }
    
    cout << "Will you play a word game? <y/n> ";
    cin  >> play;
    play = tolower(play);
    while (play == 'y') {
        target = wordlist[rand() % wordlist.size()];
        length = target.length();
        attempt = string(length, '-');
        guesses = 6;
        cout << "Guess my secret word. It has " << length 
             << " letters, and you guess\n" << "one letter at a time. You get " 
             << guesses << " wrong guesses.\n";
        cout << "Your word: " << attempt << endl;
        while (guesses > 0 && attempt != target) {
            cout << "Guess a letter: ";
            cin  >> letter;
            if (badchars.find(letter) != string::npos || attempt.find(letter) != string::npos) {
                cout << "You already guessed that. Try again.\n";
                continue;
            }
            loc = target.find(letter);
            if (loc == string::npos) {
                cout << "Oh, bad guess!\n";
                --guesses;
                badchars += letter;
            } else {
                cout << "Good guess!\n";
                attempt[loc] = letter;
                loc = target.find(letter, loc + 1);
                while (loc != string::npos) {
                    attempt[loc] = letter;
                    loc = target.find(letter, loc + 1);
                }       
            }
            cout << "Your word: " << attempt << endl;
            if (attempt != target) {
                if (badchars.length() > 0) {
                    cout << "Bad choices: " << badchars << endl;
                    cout << guesses << " bad guesses left\n";
                }
            }
        }  
        if (guesses > 0) {
            cout << "That's right!\n";
        } else {
            cout << "Sorry, the word is " << target << ".\n";
        }

        cout << "Will you play another? <y/n> ";
        cin  >> play;
        play = tolower(play);

    }
    cout << "Bye\n";
    return 0;
}

