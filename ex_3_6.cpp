#include <iostream>

using namespace std;

int main()
{
    unsigned long long gallons{}, miles{};
    std::cout << "How many miles you have driven: ";
    std::cin >> miles;
    std::cout << "How many gallons of gasoline you have used: ";
    std::cin >> gallons;
    std::cout << "Car is gotten miles per gallons: " << miles / gallons;
}

