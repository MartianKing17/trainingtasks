#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

class Store
{
public:
    Store(ofstream *fout);
    void operator()(const string &str);
private:
    ofstream * m_fout;
};

Store::Store(ofstream *fout)
{
    m_fout = fout;
}
void Store::operator()(const string &str)
{
    size_t len = str.length();
    m_fout->write((char *)&len, sizeof(size_t));
    m_fout->write(str.data(), len);
}

void ShowStr(const string &str);
void GetStrs(ifstream &fin, vector<string> & strs);

int main()
{
    vector<string> vostr;
    string temp{};
    cout << "Enter strings (empty line to quit):\n";
    getline(cin, temp);

    while (temp != "") {        
        temp += '\n';
        vostr.push_back(temp);
        getline(cin, temp);
    }

    for_each(vostr.begin(), vostr.end(), ShowStr);
    ofstream fout("strings.dat", ios_base::out | ios_base::binary);
    for_each(vostr.begin(), vostr.end(), Store(&fout));
    fout.close();
    
    vector<string> vistr;
    ifstream fin("strings.dat", ios_base::in | ios_base::binary);
    if (!fin.is_open()) {
        cerr << "Could not open file for input.\n";
        exit(EXIT_FAILURE);
    }
    GetStrs(fin, vistr);
    cout << "\nHere are the strings read from the file:\n";
    for_each(vistr.begin(), vistr.end(), ShowStr);
    return 0;
}

void ShowStr(const string &str)
{
    cout << str;
}

void GetStrs(ifstream &fin, vector<string> & strs)
{
    char * arr;
    size_t size{};
    string data;
    while(!fin.eof()) {
        fin.read((char *)&size, sizeof(size));
        arr = new char[size];
        fin.read(arr, size);
        strs.push_back(arr);
        delete[] arr;
        arr = nullptr;
    }
}


