#include "sales.h"

Sales::bad_index::bad_index(int ix, const std::string &s) 
      : logic_error(s), bi(ix) {}

Sales::Sales(int yy) : year(yy), gross{} {}
Sales::Sales(int yy, const double * gr, int n) : year(yy)
{
    int lim = (n < MONTHS) ? n : MONTHS;
    int i{};
    for (i = 0; i < lim; ++i) {
        gross[i] = gr[i];
    }

    for (; i < MONTHS; ++i) {
        gross[i] = 0;
    }
}

double Sales::operator[](int i) const
{
    if (i < 0 || i >=MONTHS) {
        bad_index *bad = new bad_index(i);
        throw bad;
    }

    return gross[i];
}

double &Sales::operator[](int i)
{
    if (i < 0 || i >=MONTHS) {
        bad_index *bad = new bad_index(i);
        throw bad;
    }
    return gross[i];
}

int Sales::bad_index::bi_val() const
{
    return bi;
}

int Sales::Year() const
{
    return year;
}

Sales::~Sales() {}

LabeledSales::LabeledSales(const std::string &lb, int yy) 
             : Sales(yy), label(lb) {}

LabeledSales::LabeledSales(const std::string &lb, int yy, const double * gr, int n)
             : Sales(yy, gr, n), label(lb) {}

double LabeledSales::operator[](int i) const
{
    if (i < 0 || i >= MONTHS) {
        nbad_index * bad = new nbad_index(Label(), i);
        throw bad;
    }

    return Sales::operator[](i);
}

double &LabeledSales::operator[](int i)
{
    if (i < 0 || i >= MONTHS) {
        nbad_index * bad = new nbad_index(Label(), i);
        throw bad;
    }

    return Sales::operator[](i);
}

LabeledSales::nbad_index::nbad_index(const std::string &lb, int ix, const std::string &s)
	     : Sales::bad_index(ix, s), lbl(lb) {}

const std::string &LabeledSales::nbad_index::label_val() const
{
    return lbl;
}

const std::string &LabeledSales::Label() const
{
    return label;
}

LabeledSales::~LabeledSales() {}
