#include <iostream>

using namespace std;

int main()
{
    const double coefForConvertingFromInchToMeters = 0.0254, coefToConvertPoundToKl = 2.2;
    const int oneFeetToInches = 12;
    int feet{}, inches{}, weight{};
    double meters, kilograms, bmi;
    std::cout << "Please, enter your height in feet and inches and your weight in pound.\n";
    std::cout << "Feet: ";
    std::cin >> feet;
    std::cout << "Inches: ";
    std::cin >> inches;
    std::cout << "Weight: ";
    std::cin >> weight;
    inches += feet * oneFeetToInches;
    meters = inches * coefForConvertingFromInchToMeters;
    kilograms = weight * coefToConvertPoundToKl;
    bmi = kilograms / std::sqrt(meters);
    std::cout << "BMI: " << bmi <<  std::endl;
}

