#include <iostream>

using namespace std;

double convertCelsiusToFahrenheit(double celcius)
{
    return 1.8 * celcius + 32.0;
}

int main()
{
    double celsius{}, fahrenheit{};
    std::cout << "Please enter a Celsius value: ";
    std::cin >> celsius;
    fahrenheit = convertCelsiusToFahrenheit(celsius);
    std::cout << celsius << " degrees Celsius is " << fahrenheit << " degrees Fahrenheit." << std::endl;
            
}

