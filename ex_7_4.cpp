#include <iostream>

using namespace std;

void enterNumberOfRange(unsigned short * arrayOfNumber, const short arrSize)
{
    cout << "Enter the five number of range 1-47:\n";
    for (int i = 0; i < arrSize; ++i) {
        cout << "The #" << (i + 1) << " number: ";
        cin >> arrayOfNumber[i];
        
        if (!cin) {
            cin.clear();
            while (cin.get() != '\n') {
                continue;
            }
            cout << "You enter not digital. Try again" << endl;
            --i;
        }

        if (arrayOfNumber[i] < 1 || arrayOfNumber[i] > 47) {
            cout << "Out of range number. Try again" << endl;
            --i;
        }
    } 
}

long double probability(unsigned short numbers, unsigned short picks)
{
    long double result = 1.0; 
    long double n;
    unsigned p;
    for (n = numbers, p = picks; p > 0; n--, p--)
        result = result * n / p ;
    return result;
}

long double pickingArray(unsigned short * arrayOfNumber, const short arrSize)
{
    long double sum{};

    for (unsigned short i = 0; i < arrSize; ++i) {
        sum += probability(47, arrayOfNumber[i]);   
    }

    return sum;
}


int main()
{ 
    const short arrSize = 5;
    unsigned short megaNumber{}, arrayOfNumber[arrSize]{};
    enterNumberOfRange(arrayOfNumber, arrSize);
    cout << "Enter a mega number in the range 1-27: ";
    cin  >> megaNumber;    
    if (!cin) {
            cin.clear();
            while (cin.get() != '\n') {
                continue;
            }
            cout << "You enter not digital. Try again" << endl;
        }

   if (megaNumber < 1 || megaNumber > 27) {
            cout << "Out of range number. Try again" << endl;
    }

    cout << "You're chaice to winning: " 
         << pickingArray(arrayOfNumber, arrSize) * probability(27, megaNumber) << endl;
}
