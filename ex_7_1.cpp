#include <iostream>

using namespace std;

double harmonic(double x, double y)
{
    return 2.0 * x * y / (x + y);
}


int main()
{
   double x{}, y{};

  do {
      cout << "Enter first number: ";
      cin  >> x;
      cout << "Enter second number: ";
      cin  >> y;
      cout << "Harmonic mean: " << harmonic(x, y) << endl;
  } while (x && y); 
}
