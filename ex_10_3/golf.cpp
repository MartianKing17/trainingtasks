#include "golf.h"
#include <iostream>
#include <cstring>

Golf::Golf(const char * name, int hc)
{
	if (strlen(name) >= len) {
        std::cout << "Character limit exceeded. Maximum: 30. Input short name" << std::endl;
    }
    strcpy(m_fullname, name);
    m_handicap = hc;
}

Golf Golf::setgolf(const char * name , int hc)
{
    if (strlen(name) >= len) {
        std::cout << "Character limit exceeded. Maximum: 30. Set default value" << std::endl;
        return *this;
    }
    strcpy(m_fullname, name);
    m_handicap = hc;
    return *this;
}

void Golf::handicap(int hc)
{
    m_handicap = hc;
}

void Golf::showgolf() const
{
    std::cout << "Fullname: " << m_fullname << "\nHandicap: " << m_handicap << std::endl;
}

