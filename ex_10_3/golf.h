
class Golf
{
public:
    Golf(const char * name  = "", int hc = 0);
    Golf setgolf(const char * name , int hc);
    void handicap(int hc);
    void showgolf() const;
private:
    static const int len = 40;
    char m_fullname[len];
    int m_handicap;
};

