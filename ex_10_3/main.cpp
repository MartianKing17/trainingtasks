#include "golf.h"
#include <iostream>
#include <cstring>

using namespace std;

void showInfo(Golf * gamers, int arrSizeGolfPlayers)
{
    cout << "Data of gamers: " << endl; 
    for (int i = 0; i < arrSizeGolfPlayers; ++i) {
        gamers[i].showgolf();
    }
}

int main()
{
    char name[40]{};
    int handicap{}, i{};
    int arrSizeGolfPlayers{};
    cout << "Enter the number of gamers: ";
    cin  >> arrSizeGolfPlayers;
    Golf * gamers = new Golf[arrSizeGolfPlayers];

    for (i = 0; i < arrSizeGolfPlayers; ++i) {
        cin.get();
        cout << "Enter the name: ";
        cin.getline(name, 40);

        if (!strcmp(name, "")) {
            cout << "Error. You enter a empty line" << endl;
            return 0;
        }

        cout << "Enter the handicap: ";
        cin  >> handicap;
        gamers[i] = {name, handicap};
    }

    showInfo(gamers, arrSizeGolfPlayers);

    for (i = 0; i < arrSizeGolfPlayers; ++i) {
        cout << "Set new handicap for #" << (i + 1) << " gamers: ";
        cin >> handicap;
        gamers[i].handicap(handicap);
    }

    showInfo(gamers, arrSizeGolfPlayers);
    delete[] gamers;
}

