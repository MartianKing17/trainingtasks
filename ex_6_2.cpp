#include <iostream>
#include <array>
#include <numeric>

using namespace std;

double findAverange(const array<double, 10> &arr)
{
   return accumulate(arr.begin(), arr.end(), 0.0) / 10.0;
}

int findAllNumLargerAverange(const array<double, 10> &arr, double averange)
{
   int number{};

   for (double num : arr) {
       if (num > averange) {
          ++number;
       }
   }

   return number;
}

int main()
{
   const size_t dArrSize = 10;
   array<double, dArrSize> arr{};
   double averange{};
   cout << "Enter a double number in the 10-digits array:\n";
   for (size_t i = 0; i < dArrSize; ++i) {
      cout << "Arr[" << i << "] = ";
      if (!(cin >> arr[i])) {
          cin.clear();
         while (cin.get() != '\n') {
             continue;
         }
         cout << "Try again" << endl;
      }
   }
   averange = findAverange(arr);
   cout << "Averange: " << averange << endl;
   cout << "Number are larger then the averange: " << findAllNumLargerAverange(arr, averange) << endl;   
}
