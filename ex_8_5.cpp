#include <iostream>

using namespace std;

template <typename T>
T max5(const T * arr)
{
    T max = arr [0];

    for (int i = 1; i < 5; ++i) {
        if (max < arr[i]) 
            max = arr[i];
    }

    return max;
}

template <typename T>
void print(const char * out, T * arr, const int size = 1)
{
    cout << out;
    for (int i = 0; i < size; ++i) {
        cout << arr[i] << ' '; 
    }
    cout << endl;
}

template <typename T>
void print(const char * out, T value)
{
    cout << out << value << endl;
}


int main()
{
    int arrInt[] = {10, 20, 5, 11, 12};
    double arrDouble[] = {25.6, 11.8, 36.7, 24.5, 11.7};
    print("Integer arr: ", arrInt, 5);
    print("Double arr: ", arrDouble, 5);
    int maxInt = max5(arrInt);
    double maxDouble = max5(arrDouble);
    print("Integer max: ", maxInt);
    print("Double max: ", maxDouble);
}

