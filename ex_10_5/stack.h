#pragma once
#include <iostream>

template <typename T>
class Stack
{
public:
    Stack(unsigned int size = 10);
    void push(T &value);
    T pop();
    bool isEmpty();
    ~Stack();
private:
    unsigned int m_top;
    unsigned int m_size;
    T * m_values;
};

template <typename T>
Stack<T>::Stack(unsigned int size):m_size(size), m_top(0)
{
    m_values = new T[m_size];
}

template <typename T>
void Stack<T>::push(T &value)
{
    if (m_top >= m_size - 1) {
        unsigned int stdAdding = 10;
        T * enlargedArr = new T[m_size + stdAdding];
        
        for (unsigned int i = 0; i < m_size; ++i) {
            enlargedArr[i] = m_values[i];
        }

        delete[] m_values;
        m_values = enlargedArr;
        m_size += stdAdding;
        enlargedArr = nullptr;
    }

    m_values[++m_top] = value;
}

template <typename T>
bool Stack<T>::isEmpty()
{
    return m_top <= 0;
}

template <typename T>
T Stack<T>::pop()
{
    if (isEmpty()) {
        std::cout << "You cannot pop. Because stack was empty" << std::endl;
        return {};
    }

    if (m_size - m_top >= 20) {
        unsigned int stdShrink = 10;
        T * shrinkToFitArr = new T[m_size - stdShrink];
        m_size -= stdShrink;
        
        for (unsigned int i = 0; i < m_size; ++i) {
            shrinkToFitArr[i] = m_values[i];
        }
        std::cout << "Maybe okey" << std::endl;
        delete[] m_values;
        m_values = shrinkToFitArr;
        shrinkToFitArr = nullptr;
    }

    return m_values[m_top--];
}

template <typename T>
Stack<T>::~Stack()
{
    delete[] m_values;
    m_values = nullptr;
}

