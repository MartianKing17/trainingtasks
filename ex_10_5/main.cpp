#include <iostream>
#include <cstring>
#include "stack.h"

using namespace std;

struct customer
{
    static const unsigned short size = 35;
    char fullname[size];
    double payment;
};

void addCustomer(Stack<customer> &stack)
{
    char fullname[customer::size];
    customer newCustomer;
    cout << "Enter the customer name(not more " << customer::size << " character): ";
    cin.get(fullname, sizeof fullname).get();
    strcpy(newCustomer.fullname, fullname);
    cout << "Enter the payment: ";
    cin  >> newCustomer.payment;
    cin.get();
    stack.push(newCustomer);
}

double rmCustomer(Stack<customer> &stack)
{
    customer lastCustomer = stack.pop();
    cout << "Customer name: " << lastCustomer.fullname << endl;
    return lastCustomer.payment;    
}

int main()
{
    char command[5]{};
    double salary{};
    Stack<customer> stack; 
    cout << "For adding customer input push, for removing pop, for quit enter q:\n";
    cout << "Enter command: ";

    while(cin.get(command, 5) && strcmp(command, "q")) {
        cin.get();
        if (!strcmp(command, "push")) {
            addCustomer(stack);
        } else if (!strcmp(command, "pop")) {
            salary += rmCustomer(stack);
            cout << "Your salary: " << salary << endl;
        } else {
            cout << "Uncorrent command" << endl;
        }   
        cout << "Enter command: ";
    }

}
