#include <cctype> 
#include <iostream>

using namespace std;

int main()
{
  char symbol{};

  do {
      cout << "Enter a symbol(except digits): ";
      cin >> symbol;
      
      if (isdigit(symbol)) {
          cout << "You're wrong. You enter a digit" << endl;
      } else {
          if (isupper(symbol)) {
              symbol = tolower(symbol); 
          } else if (islower(symbol)) {
             symbol = toupper(symbol);
          }
          cout << symbol << endl;
     }
  } while(symbol != '@');
}
