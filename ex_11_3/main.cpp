#include <iostream>
#include <cstdlib>
#include <ctime>
#include "vector.h"

using namespace std;
using VECTOR::Vector;

int main()
{
    unsigned long steps{}, attemps{}, n{}, max{}, min{}, averange{};    
    double direction{}, target{}, dstep{};
    Vector step{};
    Vector result(0.0, 0.0);
    srand(time(0));
    cout << "How many attemps do you want: ";
    cin >> attemps;
    min = attemps;
    cout << "Enter target distance: ";
    cin >> target;

    cout << "Enter step length: ";
    if(!(cin >> dstep)) {
        cout << "Uncorrect dstep" << endl;
        return 0;
    }

    while(n < attemps)
    {
        while(result.magval() < target) {
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            result = result + step;
            ++steps;
        }

        cout << "After " << steps << " steps, the subject has the following location:\n";
        cout << result << endl;
        result.polar_mode();
        cout << " or\n" << result << endl;
        cout << "Averange outward distance per step = " << result.magval()/steps << endl;

        if (max < steps) {
            max = steps;
        } 

        if (min > steps) {
            min = steps;
        }

        averange += steps;
        steps = 0;
        result.reset(0.0, 0.0);
        ++n;
    }
    cout << "Maximum steps: " << max << ", minimum steps: " << min 
         << ", averange: " << averange / attemps << endl;
    cin.clear();
    while (cin.get() != '\n')
        continue;
    return 0;
}
