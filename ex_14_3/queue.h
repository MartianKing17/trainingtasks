
template <typename T>
class Queue
{
public:
    Queue();
    ~Queue();
    bool empty() const;
    int size() const;
    void push(T *value);
    void pop();
    T * front();
    T * back();
private:
    struct Node
    {
        T * data;
        Node * next;
    };
    Node * first;
    Node * last;
    int m_size;
};

template <typename T>
Queue<T>::Queue() : first(nullptr), last(nullptr), m_size(0) {}

template <typename T>
Queue<T>::~Queue()
{
    while(empty()) {
        pop();
    }
}

template <typename T>
bool Queue<T>::empty() const
{
    return size(); 
}

template <typename T>
int Queue<T>::size() const
{
    return m_size;
}

template <typename T>
void Queue<T>::push(T *value)
{
    if (first == nullptr) {
        first = new Node;
	    first->data = value;
	    first->next = nullptr;
        last  = first;
    } else {
        Node * item = new Node;
	    item->data = value;
	    item->next = nullptr;
        last->next = item;
        last = item;
        item = nullptr;
    }
    ++m_size;
}

template <typename T>
void Queue<T>::pop()
{
    /* 
        В конце неправильно ссылается. Возможно first = null, last = item
    */
    if (first == nullptr) {
        return;
    } else {
        Node * item = first;
        first = first->next;
        delete[] item;
        item = nullptr;
    }
    --m_size;
}

template <typename T>
T * Queue<T>::front()
{
    if (first == nullptr) {
        return {};
    }

    return first->data;
}

template <typename T>
T * Queue<T>::back()
{
    if (first == nullptr) {
        return {};
    }

    return last->data;
}

