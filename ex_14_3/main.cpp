#include "queue.h"
#include "worker.h"
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    Worker * worker{};
    char choice{};
    Queue<Worker> queue{};
    
    cout << "Enter the employeer category:\n"
              << "w: waiter s: singer "
              << "t: singing waiter q: quit\n";
    while (cin >> choice && choice != 'q') {
        if (strchr("wst", choice) == 0) {
            cout << "You put don't correct character" << endl;
                continue; 
        }

        switch(choice) {
            case 'w':
                worker = new Waiter{};
                break;
            case 's':
                worker = new Singer{};
                break;
            case 't': 
                worker = new SingingWaiter{};
                break;
            default:
                break;
        }
        cin.get();
        worker->Set();
        queue.push(worker);
        worker = nullptr;
        cout << "Enter the employeer category:\n"
              << "w: waiter s: singer "
              << "t: singing waiter q: quit\n";
    }

    cout << "\nHere is your staff:\n";
    int size = queue.size();

    for (int i = 0; i < size; ++i) {
        cout << endl;
        worker = queue.front();
        queue.pop();
        worker->Show();
	    delete worker;
	    worker = nullptr;
    }

    return 0;
}

