#include <initializer_list>
#include <iostream>

using namespace std;

template<typename T>
T averange_list(const initializer_list<T> &list)
{
    int i = 0;
    T sum{};
    for (auto it = list.begin(); it != list.end(); ++it) {
        sum += *it;
        ++i;
    }
    return sum;
}

int main()
{
    auto q = averange_list({15.4, 10.7, 9.0});
    cout << q << endl;
    cout << averange_list({20, 30, 19, 17, 45, 38}) << endl;
    auto ad = averange_list<double>({'A', 70, 65.33});
    cout << ad << endl;
    return 0;
}
