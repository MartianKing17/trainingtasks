#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char * argv[])
{
    if (argv[1] == 0) {
        cout << "You don't set a file name where write data" << endl;
        return 0;
    }

    char ch{};
    ofstream fin;
    fin.open(argv[1]);

    if (!fin.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }

    cout << "Enter a data:" << endl;

    while (cin.peek() != EOF) {
        cin.get(ch);
        fin << ch;
    }

    fin.close();
    return 0;
}
