#include <iostream>

class Complex
{
public:
    Complex();
    Complex(double real, double img);
    Complex operator+(const Complex &other);
    Complex operator-(const Complex &other);
    Complex operator*(const Complex &other);
    Complex operator~();
    friend Complex operator*(double n, const Complex &other);
    friend std::ostream &operator<<(std::ostream &os, const Complex &other);
    friend std::istream &operator>>(std::istream &is, Complex &other);
private:
    double m_real;
    double m_img;
};
