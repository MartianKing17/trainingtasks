#include "complex.h"

Complex::Complex() : m_real(0), m_img(0) {}
Complex::Complex(double real, double img) : m_real(real), m_img(img) {}
Complex Complex::operator+(const Complex &other)
{
    return Complex(m_real + other.m_real, m_img + other.m_img);
}

Complex Complex::operator-(const Complex &other)
{
    return Complex(m_real - other.m_real, m_img - other.m_img);
}

Complex Complex::operator*(const Complex &other)
{
    double real = m_real * other.m_real - m_img * other.m_img;
    double img  = m_real * other.m_img + m_img * other.m_real;
    return Complex(real, img);
}
 
Complex Complex::operator~()
{
    return Complex(m_real, -m_img);
}

Complex operator*(double n, const Complex &other)
{
    return Complex(n * other.m_real, n * other.m_img);
}

std::ostream &operator<<(std::ostream &os, const Complex &other)
{
    os << '(' << other.m_real << ", " << other.m_img << "i)";
    return os; 
}

std::istream &operator>>(std::istream &is, Complex &other)
{
    std::cout << "real: ";
    is >> other.m_real;

    if (!is) {
        return is;
    }

    std::cout << "imaginary: ";
    is >> other.m_img;
    return is;
}
