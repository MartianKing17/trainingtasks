#include <iostream>

using namespace std;

double convertLightYearToAstronomicalUnits(double lightYears)
{
    double oneLigthYearInAstronomicalUnits = 63240;
    return lightYears * oneLigthYearInAstronomicalUnits;
}

int main()
{
    double lightYears{}, astronomicalUnits{};
    std::cout << "Enter the number of light years: ";
    std::cin >> lightYears;
    astronomicalUnits = convertLightYearToAstronomicalUnits(lightYears);
    std::cout << lightYears << " light years = " << astronomicalUnits 
              << " astronomical units." << std::endl;         
}

