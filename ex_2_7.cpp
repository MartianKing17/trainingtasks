#include <iostream>

using namespace std;

int main()
{
    int hours{}, minutes{};
    std::cout << "Enter the number of hours: ";
    std::cin >> hours;
    std::cout << "Enter the number of minutes: ";
    std::cin >> minutes;
    std::cout << "Time: " << hours << ':' << minutes << std::endl;      
}

