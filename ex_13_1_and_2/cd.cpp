#include "cd.h"
#include <cstring>
#include <iostream>

Cd::Cd() : perfomers(nullptr), label(nullptr), selections(0), playtime(0.0)
{
    perfomers = new char[10]{};
    label = new char[10]{};
}

Cd::Cd(const Cd &d)
{
    int perLen = std::strlen(d.perfomers);
    int labelLen = std::strlen(d.label);
    perfomers = new char[perLen + 1];
    label = new char[labelLen + 1];
    std::strcpy(perfomers, d.perfomers);
    std::strcpy(label, d.label);
    selections = d.selections;
    playtime = d.playtime;
}

Cd::Cd(const char * s1, const char * s2, int n, double x)
{
    int perLen = std::strlen(s1);
    int labelLen = std::strlen(s2);
    perfomers = new char[perLen + 1];
    label = new char[labelLen + 1];
    std::strcpy(perfomers, s1);
    std::strcpy(label, s2);
    selections = n;
    playtime = x;
}

Cd::~Cd()
{
    delete[] perfomers;
    delete[] label;
}

void Cd::Report() const
{
    std::cout << "Perfomers: " << perfomers
              << "\nLabel: " << label
              << "\nSelections: " << selections
              << "\nPlaytime: " << playtime << std::endl;
}

Cd &Cd::operator=(const Cd &cd)
{
    delete[] perfomers;
    delete[] label;
    int perLen = std::strlen(cd.perfomers);
    int labelLen = std::strlen(cd.label);
    perfomers = new char[perLen + 1]{};
    label = new char[labelLen + 1]{};
    std::strcpy(perfomers, cd.perfomers);
    std::strcpy(label, cd.label);
    selections = cd.selections;
    playtime = cd.playtime;
    return *this;
}


