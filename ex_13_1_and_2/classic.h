#include "cd.h"

class Classic: public Cd
{
public:
    Classic();
    explicit Classic(const char * s1, const char * s2, const char * name, int n, double x);
    Classic(const Classic &c);
    ~Classic();
    void Report() const;
    Classic &operator=(const Classic &c);
private:
    char * m_name;
};
