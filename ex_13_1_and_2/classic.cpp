#include "classic.h"
#include <cstring> 
#include <iostream>

Classic::Classic(): Cd(), m_name(nullptr)
{
    m_name = new char[10]{};
}

Classic::Classic(const char * s1, const char * s2, const char * name, int n, double x)
        : Cd(s1, s2, n, x), m_name(nullptr)
{
    int len = std::strlen(name);
    m_name = new char[len + 1];
    std::strcpy(m_name, name);
}

Classic:: Classic(const Classic &c) : Cd(c), m_name(nullptr)
{
    int len = std::strlen(c.m_name);
    m_name = new char[len + 1];
    std::strcpy(m_name, c.m_name);
}

Classic::~Classic()
{
    delete[] m_name;		
}

void Classic::Report() const
{
    Cd::Report();
    std::cout <<"Name: " << m_name << std::endl;
}

Classic &Classic::operator=(const Classic &c)
{
    Cd::operator=(c);
    delete[] m_name;
    int len = std::strlen(c.m_name);
    m_name = new char[len + 1]{};
    std::strcpy(m_name, c.m_name);
    return *this;
}
