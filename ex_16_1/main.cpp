#include <string>
#include <algorithm>
#include <cctype>
#include <iostream>

using namespace std;

bool isPalindrome(const string &str);
bool isHaveIgnoreChar(const string &str);

int main()
{
    string str{};
    cout << "Enter the string <q to quit>: ";
    getline(cin, str);

    while (str != "q") {
        if (!isHaveIgnoreChar(str)) {
            cout << "We cannot analysis this string. Try again" << endl;
        } else if (isPalindrome(str)) {
            cout << "It's the palindrome\n"; 
        } else {
            cout << "It's not a palindrome\n";
        }

        cout << "Enter the string <q to quit>: ";
        getline(cin, str);
    }
}

bool isPalindrome(const string &str)
{
    bool res{};
    string palindrome{};
    copy(str.rbegin(), str.rend(), back_insert_iterator<string>(palindrome));

    if (palindrome == str) {
        res = true;
    }

    return res;
}

bool isHaveIgnoreChar(const string &str)
{
    bool res = true;
    for_each(str.begin(), str.end(), [&res](const char c) {
        if (!isalpha(c)) {
            res = false;
        }    
    });

    if (res) {
        for_each(str.begin(), str.end(), [&res](const char c) {
            if (!islower(c)) {
                res = false;
            }
        }); 
    }

    return res;
}
