#include <iostream>
#include "sales.h"
#include <iostream>

using namespace std;
using namespace sales;

void enterData(double * arr, int size, int output)
{
    std::cout << "Enter data for #" << output << " sales data\n";
    
    for(int i = 0; i < size; ++i) {
        cout << "Enter how many sales in #" << (i + 1) << " quarter: " ;
        cin >> arr[i];
    }
}



int main()
{
    const int quartes = 4;
    double arr[quartes];
    enterData(arr, quartes, 1);
    Sales s1{arr, quartes};
    enterData(arr, quartes, 2);
    Sales s2 = s1.setSales(arr, quartes);
    cout << "Information about #1 sales:\n";
    s1.showSales();
    cout << "Information about #2 sales:\n";
    s2.showSales();
}
