
namespace sales
{
    class Sales
    {
     public:
        Sales(const double arr[], int n);
        Sales setSales(const double arr[], int n);
        void showSales() const;
     private:
        static const int quartes = 4;
        double m_sales[quartes];
        double m_averange;
        double m_max;
        double m_min;
    };
}
