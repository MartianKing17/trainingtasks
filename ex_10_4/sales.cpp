#include <iostream>
#include <algorithm> 
#include <numeric>
#include "sales.h"

namespace sales
{

    Sales::Sales(const double arr[], int n)
    {
        for (int i = 0; i < n ; ++i) {
				m_sales[i] = arr[i];
		}
		m_averange = std::accumulate(m_sales, m_sales + quartes, 0.0) / quartes;
		m_max = *std::max_element(m_sales, m_sales + quartes);
		m_min = *std::min_element(m_sales, m_sales + quartes);
    }

    Sales Sales::setSales(const double arr[], int n) 
	{
        return Sales(arr, n);
	}

    void Sales::showSales() const
	{
		for (int i = 0; i < quartes; ++i) {
			std::cout << "In the quarter #" << (i + 1) << ": " << m_sales[i] << '\n';
		}

		std::cout << "Averange: " << m_averange;
		std::cout << "\nMax: "    << m_max;
		std::cout << "\nMin: "    << m_min << std::endl;
	}
}
