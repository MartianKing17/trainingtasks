#include <iostream>
#include <string>
using namespace std;

int main()
{ 
    char ch;
    string word;
    unsigned int sum{};
    cout << "Enter words (to stop, type the word done):\n";

   do {
	cin.get(ch);
	if (ch == '\r' || ch == char(32) || ch == '\n') {
		++sum;
		word.clear();
	} else {
	    word.push_back(ch);
        }
   } while (word != "done");

   cout << "You entered a total of " << sum << " words." << endl;
}
