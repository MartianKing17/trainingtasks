#include <iostream>

using namespace std;

int main()
{
    const unsigned int fromSeconsToMinutes{60}, fromMinutesToHours{60}, fromHoursToDays{24};
    unsigned long long seconds{}, days{}, hours{}, minutes{};
    std::cout << "Enter the number of seconds: ";
    std::cin >> seconds;
    minutes = seconds / fromSeconsToMinutes;
    hours = minutes / fromMinutesToHours;
    days = hours / fromHoursToDays;
    std::cout << seconds << " seconds = " << days << " days, " << days - hours 
              << " hours, " << hours - minutes << " minutes, " << minutes - seconds 
              << " seconds" << std::endl;
}

