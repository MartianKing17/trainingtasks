#include <iostream>

using namespace std;

int fill_array(double * arrBegin, double * arrEnd)
{
    double temp;
    int size{};
    double *end = arrEnd;

    for (; arrBegin != end; arrBegin = arrBegin + 1) {
        cout << "Enter value #" << (size + 1) << ": ";
        cin  >> temp;

        if (!cin) {
            cin.clear();
            while (cin.get() != '\n')
                continue;
            cout << "Bad input; input process terminated.\n";
            arrEnd = arrBegin;
            break;
        } else if (temp < 0) {
            arrEnd = arrBegin;
            break;
        }
        *arrBegin = temp;
        ++size;
    }
    return size;
}

void show_array(const double * arrBegin, const double * arrEnd) 
{
    int i = 1;
    for (; arrBegin != arrEnd; arrBegin = arrBegin + 1, ++i) {
        cout <<"Property #" << i << ": $" << *arrBegin << endl;
    }
}

void revalue(double r, double * arrBegin, double * arrEnd)
{
    for (; arrBegin != arrEnd; arrBegin = arrBegin + 1) {
        *arrBegin *= r;
    }
}

int main()
{ 
    const short max = 5;
    unsigned short size{};
    double properties[max];
    double *startArray = properties, *endArray = properties + max;
    size = fill_array(startArray, endArray);
    cout << endArray << '\n' << startArray << endl;
    show_array(startArray, endArray);
    if (size > 0) {
        cout << "Enter revaluation factor: ";
        double factor;
        while (!(cin >> factor)) {
            cin.clear();
            while (cin.get() != '\n')
                continue;
            cout << "Bad input; Please enter a number: ";
        }
        revalue(factor, startArray, endArray);
        show_array(startArray, endArray);
    }
    cout << "Done.\n";
    cin.get();
    cin.get();
    return 0;
}
