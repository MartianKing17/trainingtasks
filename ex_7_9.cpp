#include <iostream>

using namespace std;

struct student
{
    char fullname[30];
    char hobby[30];
    int ooplevel;
};

int getinfo(student pa[], int n);
void display1(student st);
void display2(const student * pa);
void display3(const student pa[], int n);

int main()
{
    cout << "Enter class size: ";
    int class_size;
    cin >> class_size;
    while (cin.get() != '\n')
        continue;
    student * ptr_stu = new student[class_size];
    int entered = getinfo(ptr_stu, class_size);
    for (int i = 0; i < entered; i++)
    {
        display1(ptr_stu[i]);
        display2(&ptr_stu[i]);
    }
    display3(ptr_stu, entered);
    delete [] ptr_stu;
    cout << "Done\n";
    return 0;
}

int getinfo(student pa[], int n)
{
    for (int i = 0; i < n; ++i) {
        cout << "Enter student fullname: ";
        cin.getline(pa[i].fullname, 30);

        if (!cin) {
            cin.clear();
            while (cin.get() != '\n')
                continue;
            return i;
        }

        if (pa[i].fullname == "") {
            return i;
        }

        cout << "Enter student hobby: ";
        cin  >> pa[i].hobby;
        cout << "Enter student ooplevel: ";
        cin  >> pa[i].ooplevel;         
        cin.get();
    }
}

void display1(student st)
{
    cout << "Fullname: " << st.fullname << "\nHobby: " << st.hobby 
         << "\nOoplevel: " << st.ooplevel << endl;
}

void display2(const student * pa)
{
    cout << "Fullname: " << pa->fullname << "\nHobby: " << pa->hobby 
         << "\nOoplevel: " << pa->ooplevel << endl;
}

void display3(const student pa[], int n)
{
    for(int i = 0; i < n; ++i) {
        display1(pa[i]);
    }
}
