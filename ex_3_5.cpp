#include <iostream>

using namespace std;

int main()
{
    unsigned long long worldPopulation{}, nationPopulation{};
    std::cout << "Enter the world's population: ";
    std::cin >> worldPopulation;
    std::cout << "Enter the population of the US: ";
    std::cin >> nationPopulation;
    std::cout << "The population of the US is " << nationPopulation * 100 / worldPopulation
              << "% of the world population." << std::endl;
}

