#include <iostream>
#include <cstring>

using namespace std;

template <typename T>
T maxn(const T * arr, int n)
{
    T max = arr[0];

    for (int i = 1; i < n; ++i) {
        if (max < arr[i]) { 
            max = arr[i];
        }
    }

    return max;
}

template <typename T>
const T * maxn(const T **strings, int n)
{
    int maxLen = strlen(strings[0]);
    T * res = const_cast<T*>(strings[0]);

    for (int i = 1; i < n; ++i) {
        if (maxLen < strlen(strings[i])) {
            maxLen = strlen(strings[i]);
            res = const_cast<T*>(strings[i]);
        }              
    }

    return res;
}

template <typename T>
void print(const char * out, T * arr, const int size = 1)
{
    cout << out;
    for (int i = 0; i < size; ++i) {
        cout << arr[i] << ' '; 
    }
    cout << endl;
}

template <>
void print(const char * out, const char **src, const int size)
{
    cout << out << endl;
    for (int i = 0; i < size; ++i) {
        cout << src[i] << endl; 
    }
}

template <typename T>
void print(const char * out, const T value)
{
    cout << out << value << endl;
}

template <typename T>
void print(const char * out, const T * str)
{
    cout << out << str << endl;
}


int main()
{
    int arrInt[] = {10, 20, 5, 11, 12, 30, 15};
    double arrDouble[] = {25.6, 11.8, 36.7, 24.5, 11.7, 36.8};
    const char * strings[50] = {"Hello world!", "Some string", "The larger text in array", "Some text"};
    print("Integer arr: ", arrInt, 7);
    print("Double arr: ", arrDouble, 6);
    print("Strings arr: ", strings, 4);
    int maxInt = maxn(arrInt, 7);
    double maxDouble = maxn(arrDouble, 6);
    const char * maxString = maxn(strings, 4);
    print("Integer max: ", maxInt);
    print("Double max: ", maxDouble);
    print<>("String max: ", maxString);
}

