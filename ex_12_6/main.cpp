#include <iostream>
#include <cstdlib>
#include <ctime>
#include "queue.h"
const int min_per_hr = 60;

bool newcustomer(double x);

int main()
{
    using std::cin;
    using std::cout;
    using std::endl;
    using std::ios_base;

    long cyclelimit{}, turnaways{}, customers{}, served{}, sum_line{}, wait_time{}, line_wait{};
    double min_per_cust{}, perhour = 200;
    int qs{}, hours = 200; 
    Item temp;
    std::srand(std::time(0));
    cout << "Case Study: Bank of Heather Automatic Teller\n";
    cout << "Enter maximum size of queue: ";
    cin  >> qs;
    Queue line_1(qs), line_2(qs);	
    cyclelimit = min_per_hr * hours;

    do {
	    turnaways = customers = served = sum_line = wait_time = line_wait = 0;
	    min_per_cust = min_per_hr / perhour;
 
        for (int cycle = 0; cycle < cyclelimit; ++cycle) {
            if (newcustomer(min_per_cust)) {
		        if (line_1.isFull() && line_2.isFull()) {
			        turnaways++;
		        } else {
                    if (line_1.queueCount() < line_2.queueCount()) {
                        temp.set(cycle);
                        line_1.enqueue(temp);
                    } else {
                        temp.set(cycle);
                        line_2.enqueue(temp);
                    }
                customers++;			
	            }
            }

            if (wait_time <= 0 && !line_1.isEmpty()) {
                line_1.dequeue(temp);
                wait_time = temp.ptime();
                line_wait += cycle - temp.when();
                served++;
            }

            if (wait_time <= 0 && !line_2.isEmpty()) {
                line_2.dequeue(temp);
                wait_time = temp.ptime();
                line_wait += cycle - temp.when();
                served++;
            }

            if (wait_time > 0) {
                wait_time--;
            }

            sum_line += line_1.queueCount() + line_2.queueCount();
        }
        --perhour;

    } while ((line_wait / served != 1));


    if (customers > 0) {
        cout << "customer accepted: " << customers << endl;
        cout << "customer served: "   << served << endl;
        cout << "turnaways: " << turnaways << endl;
        cout << "averange queue size: ";
        cout.precision(2);
        cout.setf(ios_base::fixed, ios_base::floatfield);
        cout << (double) sum_line / cyclelimit << endl;
        cout << "averange wait time: "
             << (double) line_wait / served << " minutes\n";
	cout << "Per hour: " << perhour << endl;
    } else {
        cout << "No customes!\n";
    }   
    cout << "Done!\n";
    return 0;
}

bool newcustomer(double x)
{
    return (std::rand() * x / RAND_MAX < 1);
}
