#pragma once

class Customer
{
public:
    Customer();
    void set(long when);
    long when() const;
    int ptime() const;
private:
    long arrive;
    int processtime; 
};

typedef Customer Item;

class Queue
{
public:
    Queue(int qs = size);
    ~Queue();
    bool isEmpty() const;
    bool isFull() const;
    int queueCount() const;
    bool enqueue(const Item &item);
    bool dequeue(Item &item);
private:
    struct Node
    {
        Item item;
        Node * next;
    };

    static const int size = 10;
    Node * front;
    Node * rear;
    int items;
    const int qsize;
    Queue(const Queue &q) : qsize(0) {}
    Queue &operator=(const Queue &q) { return *this; }
};
