#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <memory>
#include <algorithm>

using namespace std;

struct Review
{
    std::string title;
    int rating;
    double price;
};

bool fillReview(Review &review);
bool operator<(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2);
bool sortByRating(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2);
bool sortByUpPrice(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2);
bool sortByDownPrice(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2);
void display(const vector<shared_ptr<Review>> &books);

int main()
{  
    int choose{};
    vector<shared_ptr<Review>> books, sortedVec;
    Review review{};
    while (fillReview(review)) {
        books.push_back(shared_ptr<Review>(new Review(review)));
    }

    if (books.size() <= 0) {
        cout << "You don't input data. And we cannot continue work." 
             << " The app will finishing work.\nBye" << endl;
        return 0;
    }

    copy(books.begin(), books.end(), back_inserter(sortedVec));
    cout << "How do you wanted sort output" 
         << " (1 - original order, 2 - alphabet order, 3 - ascending order of rating, "
         << "4 - ascending order prices, 5 - in order of decreasing price, q to quit): ";

    while (cin >> choose) {
        switch(choose) {
            case 1:
                break;
            case 2:
                sort(sortedVec.begin(), sortedVec.end());
                break;
            case 3: 
                sort(sortedVec.begin(), sortedVec.end(), sortByRating);
                break;
            case 4:
                sort(sortedVec.begin(), sortedVec.end(), sortByUpPrice);
                break;
            case 5: 
                sort(sortedVec.begin(), sortedVec.end(), sortByDownPrice);
                break;                               
        }
        display(sortedVec);
        cout << "How do you wanted sort output" 
         << " (1 - original order, 2 - alphabet order, 3 - ascending order of rating, "
         << "4 - ascending order prices, 5 - in order of decreasing price, q to quit): ";
    }

    cout << "Bye" << endl;
    return 0;
}

bool fillReview(Review &review)
{
    cout << "Enter book title (q to quit): ";
    getline(cin, review.title);
    if (review.title == "q") {
        return false;
    }
    cout << "Enter book rating: ";
    cin >> review.rating;

    if(!cin) {
        return false;
    }

    cout << "Enter book price: ";
    cin  >> review.price;

    if(!cin) {
        return false;
    }

    while (cin.get() != '\n') {
        continue;
    }

    return true;
}

bool operator<(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2)
{
    return r1->title < r2->title;
}

bool sortByRating(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2)
{
    return r1->rating < r2->rating;
}

bool sortByUpPrice(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2)
{
    return r1->price < r2->price;
}

bool sortByDownPrice(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2)
{
    return r1->price > r2->price;
}


void display(const vector<shared_ptr<Review>> &books)
{
    for (const auto &value : books) {
        cout << "Name: " << value->title 
             << "\nRating: " << value->rating
             << "\nPrice: " << value->price << endl;
    }
}
