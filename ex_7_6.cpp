#include <iostream>
#include <algorithm>

using namespace std;

void fill_array(double * array, const unsigned int size)
{
    cout << "Enter the number in double array:\n";
   
    for (unsigned int i = 0; i < size; ++i) {
        cout << "Enter the #" << (i + 1) << ": ";
        cin  >> array[i];

        if (!cin) {
            cin.clear();
            while (cin.get() != '\n') {
                continue;
            }
            --i;
        }
    } 
}

void show_array(double * array, const unsigned int size) 
{
    for (unsigned int i = 0; i < size; ++i) {
        cout << "#" << (i + 1) << " element of array: " << array[i] << '\n';
    }
}

void reverse_array(double * array, const unsigned int size, unsigned int i) 
{
    unsigned int range = size / 2;
   
    if (i >= range) {
        cout << "Uncorrect values" << endl;
        return;
    }
   
    for (; i < range; ++i) {
        swap(array[i], array[size - 1 - i]);
    }
}

int main()
{
    unsigned int size{};
    cout << "Enter size of array: ";
    cin  >> size;
    double array[size]{};
    fill_array(array, size);
    cout << "Array before reverse:\n";
    show_array(array, size);
    reverse_array(array, size, 1);
    cout << "Array after reverse:\n";
    show_array(array, size);
}
