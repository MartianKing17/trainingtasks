#include <iostream>
#include <string>

using namespace std;

const int arSize = 10;
void strcount(string str);

int main()
{
    string input{};
    cout << "Enter a line:\n";
    getline(cin, input);

    do {    
      strcount(input);
      cout << "Enter next line (empty line to quit):\n";
      getline(cin, input);
    } while (input != "");    
   
    cout << "Bye" << endl;
    return 0;
}

void strcount(string str)
{
    static int total = 0;
    int count = 0;
    cout << "\"" << str << "\" contains ";
    count = str.size();
    total += count;
    cout << count << " characters\n";
    cout << total << " characters total\n"; 
}
