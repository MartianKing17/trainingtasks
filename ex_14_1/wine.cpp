#include "wine.h"
#include <iostream>
#include <algorithm>

Wine::Wine() : m_label(""), m_info{5, 5} {}

Wine::Wine(const char * label, int year, const int years[], const int bottles[])
     : m_label(label), m_info{year, year}
{
    for(int i = 0; i < year; ++i) {
        m_info.first[i]  = years[i];
        m_info.second[i] = bottles[i];
    }
}

Wine::Wine(const char * label, int year)
     : m_label(label), m_info{year, year} {}

void Wine::getBottles()
{
    std::cout << "Enter " << m_label << " data for " 
              << m_info.first.size() << " year(s):\n";

    for(int i = 0; i < m_info.first.size(); ++i) {
        std::cout << "Enter year: ";
        std::cin  >> m_info.first[i];
        std::cout << "Enter bottles for that year: ";
        std::cin  >> m_info.second[i];
    }
}

std::string Wine::label() const
{
    return m_label;
}

void Wine::show() const
{
    std::cout << "Wine: " << m_label << '\n';
    std::cout.width(10);
    std::cout << "Year";
    std::cout.width(12);
    std::cout <<"Bottles\n";

    for(int i = 0; i < m_info.first.size(); ++i) {
        std::cout.width(10);
        std::cout << m_info.first[i];
        std::cout.width(8);
        std::cout << m_info.second[i] << '\n';
    }
}

int Wine::sum()
{
    return m_info.second.sum();
}

