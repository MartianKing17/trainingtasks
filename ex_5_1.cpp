#include <iostream>

using namespace std;

int main()
{
    int startRange{}, endRange{}, sum{};
    cout << "Input start range: ";
    cin >> startRange;
    cout << "Input end range: ";
    cin >> endRange;
    
    for (;startRange <= endRange; ++startRange)
 	sum += startRange;

    cout << "Sum is " << sum << endl;
}
