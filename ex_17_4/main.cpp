#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

void readFile(ifstream &fin, vector<string> &vector)
{
    string data{};

    while (!fin.eof()) {
        getline(fin, data);
        data += " ";
        vector.push_back(data);
    }

    fin.close();
}

int main()
{
    string data{};
    vector<string> data1{};
    vector<string> data2{};
    ifstream fin1, fin2;
    string filename{};
    cout << "Enter a filename first file: ";
    cin  >> filename;
    fin1.open(filename);
    if (!fin1.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }
    
    cout << "Enter a filename second file: ";
    cin  >> filename;
    fin2.open(filename);
    if (!fin2.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }
    cout << "Enter a filename output file: "; 
    cin  >> filename;

    readFile(fin1, data1);
    readFile(fin2, data2);
    
    ofstream fout(filename);
    if(!fout.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }   

    int size = data1.size() > data2.size() ? data1.size() : data2.size();

    for (int i = 0; i < size; ++i) {
        if (i < data1.size()) {
            data += data1.at(i);
        } 

        if (i < data2.size()) {
            data += data2.at(i);
            data += '\n';
        }
        
        fout.write(data.c_str(), data.size());
        data.clear();
    }
    
    fout.close();
    return 0;
}
