#include "move.h"
#include <iostream>

using namespace std;

int main()
{
    double x{}, y{};
    cout << "Enter x: ";
    cin  >> x;
    cout << "Enter y: ";
    cin  >> y;
    Move move(x, y);
    move.showmove();
    cout << "Enter new x: ";
    cin  >> x;
    cout << "Enter new y: ";
    cin  >> y;
    move.reset(x, y);
    move.showmove();
    cout << "Enter another x: ";
    cin  >> x;
    cout << "Enter another y: ";
    cin  >> y;
    Move anotherMove(x, y);
    anotherMove.showmove();
    move = move.add(anotherMove);
    cout << "Result of add the last pointer: ";
    move.showmove();
}

