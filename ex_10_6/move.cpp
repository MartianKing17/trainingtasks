#include "move.h"
#include <iostream>


Move::Move(double x, double y) : m_x(x), m_y(y) {};

void Move::showmove() const
{
    std::cout << "x = " << m_x << ", y = " << m_y << std::endl;
}

Move Move::add(const Move & m) const
{
    return Move(m.m_x + m_x, m.m_y + m_y);
}
 
void Move::reset(double x, double y)
{
    m_x = x;
    m_y = y;
}
