#pragma once

class Move
{
 public:
    Move(double x = 0, double y = 0);
    void showmove() const;
    Move add(const Move & m) const;
    void reset(double x = 0, double y = 0);
 private:
    double m_x;
    double m_y;
};
