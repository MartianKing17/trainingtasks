#pragma once
#include <iostream>

class Time
{
public:
    Time();
    Time(int h, int m = 0);
    void addMin(int m);
    void addHr(int h);
    void reset(int h = 0, int m = 0);
    friend Time operator+(const Time &t1, const Time &t2);
    friend Time operator-(const Time &t1, const Time &t2);
    friend Time operator*(const Time &t1, double n);
    friend Time operator*(double n, const Time &t1);
    friend std::ostream &operator<<(std::ostream &os, const Time &t);
private:
    int m_hours;
    int m_minutes;
};
