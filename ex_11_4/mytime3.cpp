#include "mytime3.h"

Time::Time() : m_hours(0), m_minutes(0) {}
Time::Time(int h, int m): m_hours(h), m_minutes(m) {}
void Time::addMin(int m)
{
    m_minutes += m;
    m_hours += m_minutes / 60;
    m_minutes %= 60;
}

void Time::addHr(int h)
{
    m_hours += h;
}

void Time::reset(int h, int m)
{
    m_hours = h;
    m_minutes = m;
}

Time operator+(const Time &t1, const Time &t2)
{
    Time sum{};
    sum.m_minutes = t1.m_minutes + t2.m_minutes;
    sum.m_hours = t1.m_hours + t2.m_hours + sum.m_minutes / 60;
    sum.m_minutes %= 60;
    return sum;
}

Time operator-(const Time &t1, const Time &t2)
{
    Time diff{};
    int tot1{}, tot2{};
    tot1 = t1.m_minutes + 60 * t1.m_hours;
    tot2 = t2.m_minutes + 60 * t2.m_hours;
    diff.m_minutes = (tot2 - tot1) % 60;
    diff.m_hours = (tot2 - tot1) / 60;
    return diff;
}

Time operator*(const Time &t1, double n)
{
    Time result{};
    long totalminutes = t1.m_hours * n * 60 + t1.m_minutes * n;
    result.m_hours = totalminutes / 60;
    result.m_minutes = totalminutes % 60;
    return result;
}

Time operator*(double n, const Time &t1)
{
    return t1 * n;
}

std::ostream &operator<<(std::ostream &os, const Time &t)
{
    os << t.m_hours << " hours, " << t.m_minutes << " minutes";
    return os;
}
