#include <iostream>
#include <cstring>

using namespace std;

struct stringy 
{
    char * str;
    int ct;
};

void set(stringy &target, const char * src);
void show(const char * str, const int n = 6);
void show(const stringy & str, const int n = 6);

int main()
{
    stringy beany;
    char testing[] = "Reality isn't what it used to be.";
    set(beany, testing);
    show(beany);
    show(beany, 2);
    testing[0] = 'D';
    testing[1] = 'u';
    show(testing);
    show(testing, 3);
    show("Done!");
    return 0;
}

void set(stringy &target, const char * src)
{
    int len = strlen(src);
    target.str = new char[len];
    target.ct = len;
    strcpy(target.str, src); 
}

void show(const char * str, const int n)
{
    for (int i = 0; i < n; ++i) {
        cout << str[i];
    }
    cout << endl;
}

void show(const stringy & str, const int n)
{
    for (int i = 0; i < n; ++i) {
        cout << str.str[i];
    }
    cout << endl;
}
