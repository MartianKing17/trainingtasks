#include "tv.h"
#include <iostream>

Tv::Tv(int s, int mc)
   : state(s), maxchannel(mc), channel(2), mode(Cable), input(TV) {}

void Tv::onoff()
{
    state ^= 1;
}

bool Tv::ison() const
{
    return state == On;
}

bool Tv::volup()
{
    if (volume < MaxVal) {
        volume++;
        return true;
    }

    return false;
}

bool Tv::voldown()
{
    if (volume > MinVal) {
        volume--;
        return true;
    } 

    return false;
}

void Tv::chanup()
{
    if (channel < maxchannel) {
        channel++;
    } else {
        channel = 1;
    }
}

void Tv::chandown()
{
    if (channel > 1) {
        channel--;
    } else {
        channel = maxchannel;
    }
}

void Tv::set_mode()
{
    mode ^= 1;
}

void Tv::set_input()
{
    input ^= 1;
}

void Tv::settings() const
{
    std::cout << "Tv is " << (state == Off ? "Off" : "On") << std::endl;
    if (state == On) {
        std::cout << "Volume setting = " << volume << std::endl;
        std::cout << "Channel setting = " << channel << std::endl;
        std::cout << "Mode = " << (mode == Antenna ? "antenna" : "cable") << std::endl;
        std::cout << "Input = " << (input == TV ? "TV" : "DVD") << std::endl;
    }
}

void Tv::changeRemoteMode(Remote &r)
{
    if (ison()) {
        r.remoteMode ^= 1;
    }
}


Remote::Remote(int m) : mode(m), remoteMode(Normal) {}

bool Remote::volup(Tv &t)
{
    return t.volup();
}

bool Remote::voldown(Tv &t)
{
    return t.voldown();
}

void Remote::onoff(Tv &t)
{
    t.onoff();
}

void Remote::chanup(Tv &t)
{
    t.chanup();
}

void Remote::chandown(Tv &t)
{
    t.chandown();
}

void Remote::set_chan(Tv &t, int c)
{
    t.channel = c;
}

void Remote::set_mode(Tv &t)
{
    t.set_mode();
}

void Remote::set_input(Tv &t)
{
    t.set_input();
}

void Remote::show_mode()
{
    std::cout << "Remote mode = " << (remoteMode == Normal ? "normal" : "interact") << std::endl;
}



