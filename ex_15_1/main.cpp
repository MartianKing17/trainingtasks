#include "tv.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
    string choice{};
    Tv tv;
    tv.onoff();
    int c{};
    Remote remote;
    cout << "What do you want(cup - channel up, cdown - channel down, "
         << "volup - volume up, voldown - volume dowm, ctvmode - change tv mode, "
         << "si - set input, crm- change remote mode, sc - set channel, off - off tv): "; 
    
    while (tv.ison()) {
        cin >> choice;

        if (choice == "cup") {
            remote.chanup(tv);
        } else if (choice == "cdown") {
            remote.chandown(tv);
        } else if (choice == "volup") {
             if(!remote.volup(tv)) {
                cout << "This maximum volume" << endl;
            }
        } else if (choice == "voldown") {   
            if(!remote.voldown(tv)) {
                cout << "This minimum volume" << endl;
            }
        } else if (choice == "ctvmode") {
            remote.set_mode(tv);
        } else if (choice == "si") {
            remote.set_input(tv);
        } else if (choice == "crm") {
            tv.changeRemoteMode(remote);
        } else if (choice == "sc") {
            cout << "Input channel number: ";
            cin  >> c;
            remote.set_chan(tv, c);
        } else if (choice == "off") {
            tv.onoff();
        } else {
            cout << "Uncorrect command. Try again" << endl;
        }     
 
	cout << "What do you want(cup - channel up, cdown - channel down, "
         << "volup - volume up, voldown - volume dowm, ctvmode - change tv mode, "
         << "si - set input, crm- change remote mode, sc - set channel, off - off tv): "; 
    }

    return 0;
}
