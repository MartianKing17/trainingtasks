#include <string>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <iostream>

using namespace std;

bool isPalindrome(const string &str);
string parse(const string &str);

int main()
{
    string str{};
    cout << "Enter the string <q to quit>: ";
    getline(cin, str);

    while (str != "q") {
	str = parse(str);
        
	if (isPalindrome(str)) {
            cout << "It's the palindrome\n"; 
        } else {
            cout << "It's not a palindrome\n";
        }

        cout << "Enter the string <q to quit>: ";
        getline(cin, str);
    }
}

bool isPalindrome(const string &str)
{
    bool res{};
    string palindrome{};
    copy(str.rbegin(), str.rend(), back_insert_iterator<string>(palindrome));

    if (palindrome == str) {
        res = true;
    }

    return res;
}

string parse(const string &str)
{
    string parsed{};
    copy_if(str.begin(), str.end(), inserter<string>(parsed, parsed.begin()),[](const char c)->bool {
        if (!strchr("\'\",.!?\n\r ", c)) {
            return true;
        }
        return false;
    });
    
    for_each(parsed.begin(), parsed.end(), [](char &c) {
        if (!islower(c)) {
            c = tolower(c);
        }
    }); 

    return parsed;
}
