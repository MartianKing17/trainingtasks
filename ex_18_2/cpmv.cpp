#include <iostream>
#include "cpmv.h"

Cpmv::Cpmv()
{
    pi = new Info{"", ""};
    std::cout << "Constructor was create" << std::endl;
}

Cpmv::Cpmv(std::string q,  std::string z) : pi(nullptr)
{
    pi = new Info{q, z};
    std::cout << "Constructor was create" << std::endl;
}

Cpmv::Cpmv(const Cpmv &cp)
{
    pi = new Info;
    pi->qcode = cp.pi->qcode;
    pi->zcode = cp.pi->zcode;    
    std::cout << "Copy constructor was create" << std::endl;
}

Cpmv::Cpmv(Cpmv && mv) : pi(mv.pi)
{
    mv.pi = nullptr;
    std::cout << "Move constructor was create" << std::endl;
}
 
Cpmv::~Cpmv()
{
    delete pi;
    std::cout << "Destructor was called" << std::endl;
}

Cpmv &Cpmv::operator=(const Cpmv &cp)
{
    if (pi == nullptr) {
        pi = new Info{cp.pi->qcode, cp.pi->zcode};
    } else {
        pi->qcode = cp.pi->qcode;
        pi->zcode = cp.pi->zcode;
    }

    std::cout << "Was using copy operator=" << std::endl;
    return *this;
}

Cpmv &Cpmv::operator=(Cpmv &&cp)
{
	delete pi;
    pi = cp.pi;
    cp.pi = nullptr;
    std::cout << "Was using move operator=" << std::endl;
    return *this;
}

void Cpmv::operator+(const Cpmv &obj) const
{
    pi->qcode += obj.pi->qcode;
    pi->zcode += obj.pi->zcode;
    std::cout << "Using operator+" << std::endl;
}

void Cpmv::Display() const
{
    std::cout << "q: "<< pi->qcode << ", z: " << pi->zcode << std::endl; 
}

