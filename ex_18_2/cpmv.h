#include <string>

class Cpmv
{
public:
    Cpmv();
    Cpmv(std::string q, std::string z);
    Cpmv(const Cpmv &cp);
    Cpmv(Cpmv && mv);
    ~Cpmv();
    Cpmv & operator=(const Cpmv &cp);
    Cpmv & operator=(Cpmv && mv);
    void operator+(const Cpmv &obj) const;
    void Display() const;
public:
    struct Info
    {
        std::string qcode;
        std::string zcode;
    };
private:
    Info *pi;
};
