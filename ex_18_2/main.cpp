#include "cpmv.h"
#include <iostream>

using namespace std;

int main()
{
    string qcode{}, zcode{};
    cout << "Enter qcode for first Cpmv: ";
    cin  >> qcode;
    cout << "Enter zcode for first Cpmv: ";
    cin  >> zcode;
    Cpmv c(qcode, zcode);
    c.Display();
    cout << "Enter qcode for second Cpmv: ";
    cin  >> qcode;
    cout << "Enter zcode for second Cpmv: ";
    cin  >> zcode;
    c = Cpmv(qcode, zcode);
    c.Display();

    Cpmv d(c);
    d.Display();
    c = d;
    c + d;
    c.Display();
    return 0;
}
