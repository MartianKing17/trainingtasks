#include <string>

class Person
{
private:
    static const int limit = 25;
    std::string m_lname;
    char m_fname[limit];
public:
    Person();
    Person(const std::string & ln, const char * gn = "Heyyou");
    void show() const;
    void formalShow() const;
};
