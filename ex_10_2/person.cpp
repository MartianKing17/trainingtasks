#include "person.h"
#include <cstring>
#include <iostream>

Person::Person()
{
    m_lname = "";
    m_fname[0] = '\0';
}

Person::Person(const std::string & ln, const char * gn)
{
    m_lname = ln;
    
    if (strlen(gn) > limit) {
        std::cout << "The bigger name. Set default name" << std::endl;
        strcpy(m_fname, "Heyyou");
    } else {
        strcpy(m_fname, gn);
    }
}

void Person::show() const
{
    std::cout << "Name: " << m_lname << "\nLastname: " << m_fname << '\n' << std::endl;
}

void Person::formalShow() const
{
    std::cout << "Lastname: " << m_fname << "\nName: " << m_lname << '\n' << std::endl;
}
