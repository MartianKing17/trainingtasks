#include <iostream>
#include <string>

using namespace std;

int main()
{
  int arraySize = 50;
  char firstName[arraySize];
  char lastName[arraySize];
  cout << "Enter your name: ";
  cin.getline(firstName, arraySize);
  cout << "Enter your last name: ";
  cin.getline(lastName, arraySize);  
  cout << "Here’s the information in a single string: " << lastName 
       << ", " << firstName << std::endl;
  return 0;
}
