#include <vector>
#include <list>
#include <chrono>
#include <algorithm>
#include <numeric>
#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
    srand(time(0));
    const int size = 10'000'000;
    vector<int> vi0(size), vi;
    list<int> li;
    iota(vi0.begin(), vi0.end(), 1);
    random_shuffle(vi0.begin(), vi0.end());
    copy(vi0.begin(), vi0.end(), back_inserter(vi));
    copy(vi0.begin(), vi0.end(), back_inserter(li));
    auto start = chrono::system_clock::now();
    sort(vi.begin(), vi.end());
    auto end = chrono::system_clock::now();
    chrono::duration<double> viRes = end - start;
    start = chrono::system_clock::now();
    li.sort();
    end = chrono::system_clock::now();
    chrono::duration<double> liRes = end - start;
    cout << "Vector sort time: " << viRes.count() 
         << ", list sort time: " << liRes.count()
         << endl;
    copy(vi0.begin(), vi0.end(), back_inserter(li));    
    start = chrono::system_clock::now();
    copy(li.begin(), li.end(), back_inserter(vi));
    sort(vi.begin(), vi.end());
    copy(vi.begin(), vi.end(), back_inserter(li));
    end = chrono::system_clock::now();
    chrono::duration<double> copyRes = end - start;
    cout << "Copy li items in vi, sorting vi, and copying vi in li: " << copyRes.count() << endl; 
    return 0;
}
