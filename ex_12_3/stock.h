#pragma once
#include <iostream>

class Stock
{
public:
    Stock();
    Stock(const char * company, long n = 0, double pr = 0.0);
    ~Stock();
    void buy(long num, double price);
    void sell(long num, double price);
    void update(double price);
    const Stock &topval(const Stock &s) const;
    friend std::ostream &operator<<(std::ostream &os, const Stock &s); 
private:
    char * m_company;
    int m_shares;
    double m_share_val;
    double m_total_val;
    void set_tot();
};
