#include <iostream>
#include <cstring>
#include "stock.h"

Stock::Stock(): m_company(nullptr), m_shares(0), m_share_val(0.0), m_total_val(0.0)
{
    m_company = new char[20]{};
    std::strcpy(m_company, "no name");
}

Stock::Stock(const char * company, long n, double pr)
      :m_company(nullptr), m_shares(0), m_share_val(pr), m_total_val(0.0)
{
    int len = std::strlen(company);
    m_company = new char[len + 1]{};
    std::strcpy(m_company, company);

    if (n < 0) {
        std::cout << "Number of shares can't be negative: "
             << m_company << " shares set to 0.\n";
        m_shares = 0;
    } else {
        m_shares = n;
    }
    set_tot();
}


Stock::~Stock()
{
    delete[] m_company;
}

void Stock::buy(long num, double price)
{
    if (num < 0) {
        std::cout << "Number of shares purchased can't be negative. "
                  << "Transaction is aborted.\n";
    } else {
        m_shares += num;
        m_share_val = price;
        set_tot();
   }
}

void Stock::sell(long num, double price)
{
    if (num < 0) {
        std::cout << "Number of shares sold can't be negative. "
                  << "Transaction is aborted.\n";
    } else if (num > m_shares) {
        std::cout << "You can't sell more than you have! "
                  << "Transaction is aborted.\n";
    } else {
        m_shares -= num;
        m_share_val = price;
        set_tot();
   }
}

void Stock::update(double price)
{
    m_share_val = price;
    set_tot();
}

const Stock &Stock::topval(const Stock &s) const
{
    if (s.m_total_val > m_total_val) {
        return s;
    } else {
        return *this;
    }
}

std::ostream &operator<<(std::ostream &os, const Stock &s)
{
    std::ios_base::fmtflags orig = os.setf(std::ios_base::fixed, std::ios_base::floatfield);
    std::streamsize prec = os.precision(3);
    os << "Company: " << s.m_company << " Shares: " << s.m_shares << '\n';
    os << "Shares price: $" << s.m_share_val; 
    os.precision(2);
    os << " Total Worth: $" << s.m_total_val << '\n';
    os.setf(orig, std::ios_base::floatfield);
    os.precision(prec);
    return os;
}

void Stock::set_tot()
{
    m_total_val = m_shares * m_share_val;
}


