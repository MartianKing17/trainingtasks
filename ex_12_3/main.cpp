#include "stock.h"
#include <iostream>

using namespace std;

int main()
{
    int st{};
    const int size = 4;
    Stock stocks[size] = { 
                        Stock("NanoSmart", 12, 20.0),
                        Stock("Boffo Objects", 200, 2.0),
                        Stock("Monolithic", 130, 3.25),    
                        Stock("Fleep Enterprises", 60, 6.5)
                        };

    cout << "Stock holdings:\n";
    for (st = 0; st < size; ++st) {
        cout << stocks[st];
    }

    const Stock * top = &stocks[0];

    for (st = 1; st < size; ++st) {
        top = &top->topval(stocks[st]);
    }

    cout << "\nMost valuable holding:\n" << *top;
    return 0;
}

