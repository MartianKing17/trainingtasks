#include "emp.h"
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <memory>

using namespace std;

shared_ptr<abstr_emp> menu();
unsigned int typeOfObject(const shared_ptr<abstr_emp> &emp);
void readFromFile(ifstream &fin, vector<shared_ptr<abstr_emp>> & employeer);

int main()
{
    string numberOfObject{};
    shared_ptr<abstr_emp> emp;
    vector<shared_ptr<abstr_emp>> employeer;
    ofstream fout;
    ifstream fin;
    fin.open("employee.dat");

    if (!fin.is_open()) {
    } else {
        readFromFile(fin, employeer);
    }

    fin.close();
    emp = menu();
    while (emp != nullptr) {
            emp->SetAll();
            employeer.push_back(move(emp));  
            emp = menu();
    }

    fout.open("employee.dat");
    if (!fout.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }

    for (const shared_ptr<abstr_emp> &e : employeer) {
        numberOfObject += to_string(typeOfObject(e)) + '\n';
        fout.write(numberOfObject.c_str(), numberOfObject.size());
        e->WriteAll(fout);
        fout.write("\n", 1);
        numberOfObject.clear();
    }

    fout.close();
    return 0;
}

shared_ptr<abstr_emp> menu()
{
    char ch{};
    shared_ptr<abstr_emp> emp;
    
    cout << "What do you want add(e - employeer, m - manager, f- add fink, "
                 << "h - add highfink) <q to quit>: ";

    cin >> ch;
    cin.get();

    if(!cin) {
        cin.clear();
        while (cin.get() != '\n') {
            continue;
        }

        return emp;
    }

    while(!strchr("emfhq", ch)) {
        cout << "You enter not correct command. Try again" << endl;
        cin >> ch;
    }

    if (ch == 'q') {
        return emp;
    }

    switch(ch) {
        case 'e':
            emp = make_shared<employee>();
            break;
        case 'm':
            emp = make_shared<manager>();
            break;
        case 'f':
            emp = make_shared<fink>();
            break;
        case 'h':
            emp = make_shared<highfink>();
            break;
        default:
            break;
    }

    return emp;
}

unsigned int typeOfObject(const shared_ptr<abstr_emp> &emp)
{
    unsigned int number{};

    if (typeid(*emp) == typeid(employee)) {
        number = 1;
    } else if (typeid(*emp) == typeid(manager)) {
        number = 2;
    } else if (typeid(*emp) == typeid(fink)) {
        number = 3;
    } else if (typeid(*emp) == typeid(highfink)) {
        number = 4;
    }

    return number;
}

void readFromFile(ifstream &fin, vector<shared_ptr<abstr_emp>> & employeer)
{
    size_t pos{};
    string fname, lname, job, reportsTo;
    int inchargeOf{};
    int number{};
    char ch{};
    shared_ptr<abstr_emp> emp{};
    string data{};
    

    while(!fin.eof()) {
        fin.get(ch).get();
        number = atoi(&ch);
        getline(fin, data);

        pos = data.find(" ");
        fname = data.substr(0, pos);
        data.erase(0, pos + 1);

        pos = data.find(" ");
        lname = data.substr(0, pos);
        data.erase(0, pos + 1);
        
        pos = data.find(" ");
        job = data.substr(0, pos);
        data.erase(0, pos + 1);

        switch(number) {
            case 1:
                emp = make_shared<employee>(fname, lname, job);
                break;
            case 2:
                pos = data.find(" "); 
                inchargeOf = atoi(data.substr(0, pos).c_str());
                data.erase(0, pos + 1);
                emp = make_shared<manager>(fname, lname, job, inchargeOf);
                break;
            case 3:
                pos = data.find(" ");
                reportsTo = data.substr(0, pos);
                data.erase(0, pos + 1);
                emp = make_shared<fink>(fname, lname, job, reportsTo);
                break;
            case 4:     
                pos = data.find(" "); 
                inchargeOf = atoi(data.substr(0, pos).c_str());
                data.erase(0, pos + 1);
                pos = data.find(" ");
                reportsTo = data.substr(0, pos);
                data.erase(0, pos + 1);
                emp = make_shared<highfink>(fname, lname, job, reportsTo, inchargeOf);
                break;
        }

        employeer.push_back(std::move(emp));
    }
}
