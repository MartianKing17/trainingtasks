#include <iostream>
#include <limits>

using namespace std;

double opAdd(double firstNumber, double secondNumber) 
{
    return firstNumber + secondNumber;
}

double opMinus(double firstNumber, double secondNumber) 
{
    return firstNumber - secondNumber;
}

double opMul(double firstNumber, double secondNumber) 
{
    return firstNumber * secondNumber;
}

double opDiv(double firstNumber, double secondNumber) 
{
    if (!secondNumber) {
        cout << "Error! Second number was 0!" << endl;
        return numeric_limits<double>::infinity();
    }
    return firstNumber / secondNumber;
}

double calculate(double firstNumber, double secondNumber, double (*func)(double, double))
{
    return func(firstNumber, secondNumber);
}

int main()
{
    int operation{};  
    double firstNumber{}, secondNumber{}, q{};
    double (*pf[4]) (double, double) {opAdd, opMinus, opMul, opDiv};
    cout << "Input first number: ";
    cin  >> firstNumber;
    cout << "Input second number: ";
    cin  >> secondNumber;
    cout << "Choose operation(add-1, minus-2, mul-3,div-4): ";
    cin  >> operation;
    q = calculate(firstNumber, secondNumber, pf[operation - 1]);
    cout << "Result: " << q << endl;
}

