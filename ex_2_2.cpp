#include <iostream>

using namespace std;

double convertFurlongsToYards(double furlongs)
{
    const double oneFurlongToYards = 220;
    return furlongs * oneFurlongToYards;
}

int main()
{
    double furlongs{}, yards{};
    std::cout << "Enter furlongs for converting to yards: ";
    std::cin >> furlongs;
    yards = convertFurlongsToYards(furlongs);
    std::cout << furlongs << " furlongs = " << yards << " yards." << std::endl;
}

