#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>

using namespace std;

template <typename T>
int reduce(T ar[], int n);

int main()
{
    int len = 20;
    long array[] =
    {
        1, 23, 12, 15, 16, 13, 14, 14, 14, 9, 11, 57, 34, 25, 86, 45, 23, 97, 99, 100
    };

    cout << "Original:\n";
    for(int i = 0; i < len; ++i) {
        cout << array[i] << ' ';
    }

    cout << endl;
    len = reduce(array, len);    
    cout << "Change original in reduce:\n";

    for(int i = 0; i < len; ++i) {
        cout << array[i] << ' ';
    }
    
    cout << endl;

    len = 5;
    string strArr[] = {"Hello world", "Robot", "Student", "Teacher", "Robot"};
    cout << "Original:\n";
    for(int i = 0; i < len; ++i) {
        cout << strArr[i] << ' ';
    }
    
    cout << endl;
    len = reduce(strArr, len);
    cout << "Change original in reduce:\n";

    for(int i = 0; i < len; ++i) {
        cout << strArr[i] << ' ';
    }
    
    cout << endl;
    return 0;
}

template <typename T>
bool equalTo(T i, T j)
{
    return i == j;
}

template <typename T>
int reduce(T ar[], int n)
{
    sort(ar, ar + n);
    auto it = unique_copy(ar, ar + n, ar);
    it = unique_copy(ar, it, ar, equalTo<T>);
    return distance(ar, it);
}
