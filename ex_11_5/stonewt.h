#pragma once
#include <iostream>

class Stonewt
{
public:
    Stonewt(double stone);
    Stonewt();
    void stage(int s = 0);
    ~Stonewt() = default;
    friend std::ostream &operator<<(std::ostream & os, const Stonewt &s);
private:
    static const int lbs_per_stn = 14;
    double m_lbs;
    int m_stage;
};
