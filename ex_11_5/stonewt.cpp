#include "stonewt.h"
#include <iostream>

Stonewt::Stonewt() : m_lbs(0), m_stage(0) {}

Stonewt::Stonewt(double lbs) : m_lbs(lbs), m_stage(0) {}

void Stonewt::stage(int s)
{
    m_stage = s;
}

std::ostream &operator<<(std::ostream &os, const Stonewt &s)
{
    int stone = int(s.m_lbs) / s.lbs_per_stn;
    int pds_left = int(s.m_lbs) % s.lbs_per_stn + s.m_lbs - int(s.m_lbs);
    switch(s.m_stage) {
        case 0:
            os << stone << " stone, " << pds_left << " pounds\n";
            break;
        case 1:
            os << int(s.m_lbs) << " pounds\n";
            break;
        case 2:
            os << s.m_lbs << " pounds\n";
            break;
	    default:
	        break;
    }

    return os;
}
