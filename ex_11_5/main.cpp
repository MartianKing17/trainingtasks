#include "stonewt.h"
#include <iostream>

using namespace std;

int main()
{
    double lbs{};
    cout << "Enter your lbs: ";
    cin  >> lbs;
    Stonewt person = lbs;
    cout << person << endl;
    person.stage(1);
    cout << person << endl;
    person.stage(2);
    cout << person << endl;
}
