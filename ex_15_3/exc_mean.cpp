#include "exc_mean.h"
#include <string>

bad_hmean::bad_hmean(double a, double b) : v1(a), v2(b), logic_error(""), msg("") {}
const char * bad_hmean::what() const noexcept
{
    msg = "hmean(" + std::to_string(v1) + ", " + std::to_string(v2) + "): invalid ";
    msg += "arguments a = -b";
    return msg.c_str();
}

bad_gmean::bad_gmean(double a, double b) : v1(a), v2(b), logic_error(""), msg("gmean() arguments should be >= 0") {}
const char * bad_gmean::what() const noexcept
{
    msg = "gmean(" + std::to_string(v1) + ", " + std::to_string(v2) + "): ";
    msg += "arguments should be >= 0";
    return msg.c_str(); 
}


