#pragma once
#include <stdexcept>
#include <string>

class bad_hmean : public std::logic_error
{
public:
    bad_hmean(double a, double b);
    const char * what() const noexcept override;
private:
    mutable std::string msg;
    double v1;
    double v2;
};

class bad_gmean : public std::logic_error
{
public:
    bad_gmean(double a, double b);
    const char * what() const noexcept override;
private:
    mutable std::string msg;
    double v1;
    double v2;
};
