#include <iostream>
#include <string>

using namespace std;

void toUpperCase(string & input)
{
    for (int i = 0; i < input.size(); ++i) {
        input.at(i) = toupper(input.at(i));
    }
}

int main()
{
    string input{};
    cout << "Enter a string (q to quit): ";
    getline(cin,input);

    while (input != "q") {
        toUpperCase(input);
        cout << input << endl;
        cout << "Next string (q to quit): ";       
        getline(cin,input);
    }
}
