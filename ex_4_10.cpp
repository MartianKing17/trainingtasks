#include <iostream>
#include <array>
#include <numeric>

using namespace std;

int main()
{
  const int size = 3;
  array<int, size> dashes;
  int summary{};
  
  for (int i = 0; i < size; ++i) {
     cout << "Enter a times of " << (i+1) << " dash: ";
     cin >> dashes[i];
  }

  for (int i = 0; i < size; ++i) {
     cout << "Times in " << (i+1) << " dash: " << dashes[i] << '\n';
  }

  summary = accumulate(dashes.begin(), dashes.end(), 1);
  cout << "Average time: " <<  summary / size << std::endl;
  return 0;
}
