#include <iostream>

using namespace std;

int convertAgesToMonth(int ages)
{
    const int monthsOfTheYear = 12;
    return ages * monthsOfTheYear;
}

int main()
{
    int age{}, months{};
    std::cout << "Enter your age: ";
    std::cin >> age;
    months = convertAgesToMonth(age);
    std::cout << "Your age in months is " << months << std::endl;
}

