#include <iostream>
#include <string>

using namespace std;

struct Pizza
{
   string namePizzaCompany;
   int diameter;
   double weight;
};

void enterPizzaData(Pizza &pizza)
{
  cout << "Input the diameter of pizza: ";
  cin >> pizza.diameter;
  cin.get();
  cout << "Input the name of pizza company: ";
  getline(cin, pizza.namePizzaCompany);
  cout << "Input the pizza weight: ";
  cin >> pizza.weight;
}

void printData(Pizza pizza)
{
  cout << "Name of pizza company: " << pizza.namePizzaCompany
       << "\nThe diameter of pizza: " << pizza.diameter
       << "\nThe pizza weight: " << pizza.weight << endl;
}

int main()
{
  int sizeOfPizzaArray;
  cout << "Enter size of data: ";
  cin >> sizeOfPizzaArray;
  Pizza * pizzas = new Pizza[sizeOfPizzaArray];

  for (int i = 0; i < sizeOfPizzaArray; ++i) {
	cin.get();
  	enterPizzaData(pizzas[i]);
  }

  cout << endl;

  for (int i = 0; i < sizeOfPizzaArray; ++i) {
  	printData(pizzas[i]);
  }

  delete[] pizzas;
  pizzas = nullptr;
  return 0;
}
