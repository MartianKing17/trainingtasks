#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

struct Contribution{
  string name;
  double amount;
};

vector<string> readFile() {
    vector<string> data{};
    string filename{}, input{};
    cout << "Enter filename: ";
    cin >> filename;
    ifstream read(filename);
   
    if (!read.is_open()) {
        cout << "Some wrong when reading file" << endl;
        return {};
    }

    while (!read.eof()) {
      getline(read, input);
      if (input.empty()) {
         continue;
      }
      data.push_back(input);
   }

   read.close();
   return data;
}

void initContributors(Contribution * contributors, vector<string> data) {
  unsigned int i = 0;
  bool isNumber{};
  for (string input : data) {
      if (!isNumber) {
          contributors[i].name = input;
          isNumber = true;
      } else {
          contributors[i].amount = stod(input);
          isNumber = false;
          ++i;
      } 
   }
}

void prepareOutput(string & donorsWhoDonateMoreMoney, string & otherDonors, Contribution * contributors, unsigned int numberOfContributors) {
   for (unsigned int i = 0; i < numberOfContributors; ++i) {
        if (contributors[i].amount >= 10000) {
		donorsWhoDonateMoreMoney += "Name: " + contributors[i].name 
                                         + ", donate: " + to_string(contributors[i].amount) + '\n';
        } else if (contributors[i].amount > 0 && contributors[i].amount < 10000) {
		otherDonors += "Name: " + contributors[i].name 
                                         + ", donate: " + to_string(contributors[i].amount) + '\n';
        }
   }
}

int main()
{
   unsigned int numberOfContributors{};
   string donorsWhoDonateMoreMoney{}, otherDonors{};
   vector<string> data = readFile();

   if (data.empty()) {
      return 0;
   }

   numberOfContributors = atoi(data.at(0).c_str());
   data.erase(data.begin());

   if (numberOfContributors <= 0) {
      cout << "Don't correct number of contribution" << endl;
      return 0;
   }

   Contribution * contributors = new Contribution[numberOfContributors]; 
   initContributors(contributors, data);
   prepareOutput(donorsWhoDonateMoreMoney, otherDonors, contributors, numberOfContributors);
   delete[] contributors;
   contributors = nullptr;

   if (!donorsWhoDonateMoreMoney.empty()) {
       cout << "Grand Patrons:\n" << donorsWhoDonateMoreMoney;
   }

   if (!otherDonors.empty()) {
       cout << "Patrons:\n" << otherDonors;
   }
   
   if (donorsWhoDonateMoreMoney.empty() || otherDonors.empty()) {
       cout << "none" << endl;
   }
}
