#include <iostream>

using namespace std;

int main()
{
    int daphneBalance = 100, cleoBalance = 100, years{};

    do {
         daphneBalance += 10;
         cleoBalance += 0.05 * cleoBalance;
         ++years;
    } while(daphneBalance > cleoBalance);

    cout << "Cleo’s investment to exceed the value of Daphne’s investment at "
         << years << " years" << endl;
}
