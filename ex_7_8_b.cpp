#include <iostream>

using namespace std;

struct Array 
{
   double expenses[4];
};

void fill(Array &expenses, const char * *(snames), const int seasons)
{
    for (int i = 0; i < seasons; i++) {
        cout << "Enter " << snames[i] << " expenses: ";
        cin >> expenses.expenses[i];
    }
}

void show(Array &expenses, const char * *(snames), const int seasons)
{
    double total = 0.0;
    cout << "\nEXPENSES\n";
    for (int i = 0; i < seasons; i++) {
    cout << snames[i] << ": $" << expenses.expenses[i] << endl;
    total += expenses.expenses[i];
    }
    cout << "Total Expenses: $" << total << endl;
}

int main()
{
    const int seasons = 4;
    const char *(snames)[seasons] = {"Spring", "Summer", "Fall", "Winter"};
    Array expenses{};
    fill(expenses, snames, seasons);
    show(expenses, snames, seasons);
    return 0;
}


