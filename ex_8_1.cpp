#include <iostream>

using namespace std;

void print(const char * str, int n = 1)
{
    for (int i = 0; i < n; ++i) {
        cout << str << endl;
    }
}

int main()
{
    const char str[] = "Hello world!";
    print(str);
    print(str, 5);
}
