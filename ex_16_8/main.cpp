#include <string>
#include <list>
#include <set>
#include <algorithm>
#include <iostream>

using namespace std;

void setFriendName(list<string> &friendList);
template <typename T>
void display(const T &friendList, const string &text);

int main()
{
    list<string> mattFriend, patFriend, friendsList;
    cout << "Enter names of Matt friend:\n";
    setFriendName(mattFriend);
    mattFriend.sort();
    cout << endl;
    display(mattFriend, "Matt friends: ");
    cout << endl;
    cout << "Enter names of Pet friend:\n";
    setFriendName(patFriend);
    patFriend.sort();
    cout << endl;
    display(patFriend, "Pat friends: "); 
    cout << endl;
    friendsList.merge(mattFriend);
    friendsList.merge(patFriend);
    set<string> friends(friendsList.begin(), friendsList.end());
    cout << endl;
    display(friends, "Matt and Pat friends: ");
    cout << endl;
    return 0;
}

void setFriendName(list<string> &friendList)
{
    string data{};
    cout << "Enter your friend name <q to quit>: ";
    getline(cin, data);
    while (data != "q") {
        friendList.push_back(data);
        cout << "Enter your friend name <q to quit>: ";
        getline(cin, data);
    }
}

template <typename T>
void display(const T &friendList, const string &text)
{
    cout << text << endl;
    for (const string &name : friendList) {
        cout << name << endl;
    }       
}
