#include <iostream>

using namespace std;

int main()
{ 
    char ch{};
    int rows{};
    cout << "Enter number of rows: ";
    cin >> rows;
    
    for (int row = 0; row < rows; ++row) {
	for (int symbol = 0; symbol < rows; ++symbol) {
		if (row + symbol < rows - 1) {
                    ch = '.';
                } else {
                    ch = '*';
                }
                cout << ch; 
        }
        cout << endl; 
    }
}
