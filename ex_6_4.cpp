#include <iostream>
#include <array>
#include <string>

using namespace std;

struct bop {
char fullname[100]; 
char title[100];
char bopname[100]; 
int preference;
};

string display(array <bop, 5> bops, char command) {
    string data{};

    for (bop _bop : bops) {
	switch (command) {
            case 'a':
                 data += string(_bop.fullname) + '\n';
                 break;
	    case 'b':
                 data += string(_bop.title) + '\n';
                 break;
	    case 'c':
                 data += string(_bop.bopname) + '\n';
                 break;
	    case 'd':
                 switch (_bop.preference) {
		     case 0:
                         data += string(_bop.fullname) + '\n';
			 break;
	             case 1:
                         data += string(_bop.title) + '\n';
			 break;
	             case 2:
                         data += string(_bop.bopname) + '\n';
			 break;
                 }
                 break;
        }
    }
   
    return data;
}

void displayInstruction() {
   cout << "Benevolent Order of Programmers Report\n";
   cout << "a. display by name";
   cout.width(30);
   cout << "b. display by title\n" << "c. display by bopname";
   cout.width(31);
   cout << "d. display by preference" << "\nq. quit" << endl;
}


int main()
{
  char command;
  string inputText = {"Enter your choice: "};
  array <bop, 5> bops = {{{"Wimp Macho", "Team Lead", "Leader", 0}, 
                         {"Raki Rhodes", "Junior Programmer", "Babies", 1},
                         {"Celia Laiter", "Middle Programmer", "MIPS", 2},
                         {"Hoppy Hipman", "Analyst Trainee", "Trainer", 1},
                         {"Pat Hand", "Senior Programmer", "LOOPY", 1} 
                        }};
  displayInstruction();
  
  while (true) {
     cout << inputText;
     cin >> command;
     
     if (command == 'q') {
         cout << "Bye!" << endl;
         break;
     } else if (command == 'a' || command == 'b' || command == 'c' || command == 'd') {
         cout << display(bops, command) << endl;
     } else {
         cout << "You enter don't available command. Try again!" << endl;
     }

     inputText = "Next choice: ";
  }
}
