#include <iostream>

using namespace std;

int main()
{
    const int oneFeetToInches = 12;
    unsigned int inches{}, feet{};
    std::cout << "Enter your height in inches:________\b\b\b\b\b\b\b\b";
    std::cin >> inches;
    feet = inches / oneFeetToInches;
    inches %= oneFeetToInches;
    std::cout << "Your height in feet and inches: " << feet << " feet and " 
              << inches << " inches." << std::endl;    
}

