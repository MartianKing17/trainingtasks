#include <iostream>
#include <string>

using namespace std;

struct CandyBar
{
    string name;
    double weight;
    int calories;
};

void set(CandyBar &candyBar, const char * name = "Millennium Munch", double weight = 2.85, int calories = 350)
{
    candyBar.name = name;
    candyBar.weight = weight;
    candyBar.calories = calories;
}

void print(const CandyBar candyBar)
{
    cout << "Name: " << candyBar.name << "\nWeight: " << candyBar.weight 
         << "\nCalories: " << candyBar.calories << endl;
}

int main()
{
    CandyBar candyBar;
    set(candyBar);
    print(candyBar);
    set(candyBar, "Snickers", 100, 350);
    print(candyBar);
}
