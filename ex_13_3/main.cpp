#include "dma.h"
#include <cstring>

using namespace std;

bool clearCin()
{
    if (!cin) {
        cout << "Bad input";
        cin.clear();
        while (cin.get() != '\n') {
            continue;
        }
        return false;
    }

    return true;
}

Base * userChoose(Base * base, const int choose)
{
    int rating{};
    char * label{}, *style{}, *color{};
    label = new char[50];
    color = new char[35];
    style = new char[50];
    cin.get();
    cout << "Enter label(maximum 50 character): ";
    cin.getline(label, 50);
    if (!clearCin()) {
        cout << "Set default" << endl;
        std::strcpy(label, "default");
    }    

    cout << "Enter rating: ";
    cin >> rating;
    if (!clearCin()) {
        cout << ". Set default" << endl;
        rating = 10;
    } 

    switch(choose) {
        case 1:
            base = new baseDMA(label, rating);   
            break;                  
        case 2:
            cin.get();
            cout << "Enter a color(no more 35 character): ";
            cin.getline(color, 35);
            if (!clearCin()) {
                cout << "Set default" << endl;
                std::strcpy(color, "blank");
            }  
            base = new lacksDMA(color, label, rating);
            break;
        case 3:
            cin.get();
            cout << "Enter a style(no more 50 character): ";
            cin.getline(style, 50);
            if (!clearCin()) {
                cout << "Set default" << endl;
                std::strcpy(style, "none");
            }
            base = new hasDMA(style, label, rating);
            break;
    }

    delete[] label;
    delete[] style;
    delete[] color;
    return base;
}

int main()
{
    int choose{};
    const int size = 4; 
    Base * base[size]{};
    
    for (int i = 0; i < size; i++) {
        cout << "Enter the what object do you want create"
             << "(1-baseDMA, 2 - lacksDMA, 3-hasDMA): ";
        cin >> choose; 
        base[i] = userChoose(base[i], choose);
    }

    
    for (int i = 0; i < size; i++) {
        base[i]->show();
    }
}
