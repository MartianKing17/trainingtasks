#include <iostream>

class Base
{
protected:
    char * m_label;
    int m_rating;
public:
    Base();
    Base(const char * label, int rating);
    Base(const Base &other);
    virtual void show() const = 0;
    virtual ~Base();
    Base &operator=(const Base &other);
    friend std::ostream &operator<<(std::ostream &os, const Base &b);
};

class baseDMA: public Base
{
public: 
    baseDMA(const char * l = "null", int r = 0);
    baseDMA(const baseDMA &rs);
    ~baseDMA() = default;
    void show() const override;
    baseDMA &operator=(const baseDMA &b);
    friend std::ostream &operator<<(std::ostream &os, const Base &b);
};

class lacksDMA: public Base
{
public:
    lacksDMA(const char * c = "blank", const char * l = "null", int r = 0);
    lacksDMA(const char * c, const Base &other);
    lacksDMA &operator=(const lacksDMA & other);
    void show() const override;
    friend std::ostream &operator<<(std::ostream &os, const lacksDMA & l);
    ~lacksDMA() = default; 
private:
    static const int m_len = 40;
    char color[m_len];
};

class hasDMA: public Base
{
public:
    hasDMA(const char * s = "none", const char * l = "null", int r = 0);
    hasDMA(const char * s, const Base &other);
    hasDMA(const hasDMA &other);
    ~hasDMA();
    void show() const override;
    hasDMA &operator=(const hasDMA &other);
    friend std::ostream &operator<<(std::ostream &os, const hasDMA &hs);
private:
    char * style;
};
