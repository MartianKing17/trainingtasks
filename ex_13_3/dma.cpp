#include "dma.h"
#include <cstring>

Base::Base() : m_label(nullptr), m_rating(10)
{
    m_label = new char[10];
}

Base::Base(const char * label, int rating) : m_label(nullptr), m_rating(rating)
{
    int len = std::strlen(label);
    m_label = new char[len + 1]{};
    std::strcpy(m_label, label); 
}

Base &Base::operator=(const Base &other)
{
    delete[] m_label;
    int len = std::strlen(other.m_label);
    m_label = new char[len + 1]{};
    std::strcpy(m_label, other.m_label); 
    m_rating = other.m_rating;
    return *this;
}

Base::Base(const Base &other) : m_label(nullptr), m_rating(other.m_rating)
{
    int len = std::strlen(other.m_label);
    m_label = new char[len + 1]{};
    std::strcpy(m_label, other.m_label); 
}

Base::~Base()
{
    delete[] m_label;    
}   

std::ostream &operator<<(std::ostream &os, const Base &b)
{
    os << "Label: " << b.m_label << ", rating: " << b.m_rating << std::endl;
    return os;
}

baseDMA::baseDMA(const char * l, int r) : Base(l, r) {};
baseDMA::baseDMA(const baseDMA &rs) : Base(rs) {};

baseDMA &baseDMA::operator=(const baseDMA &b)
{
    Base::operator=(b);
    return *this;
}

void baseDMA::show() const
{
    std::cout << (const Base &) *this;
}

lacksDMA::lacksDMA(const char * c, const char * l, int r)
         : Base(l, r), color("black")
{
    int len = std::strlen(c);
    if (len < m_len) {
        std::strcpy(color, c);
    } else {
        std::cout << "You try set to color array more " << len 
                  << " character.Will be set default values." << std::endl;
    }
}

lacksDMA::lacksDMA(const char * c, const Base &other) : Base(other), color("blank")
{
    int len = std::strlen(c);
    if (len < m_len) {
        std::strcpy(color, c);
    } else {
        std::cout << "You try set to color array more " << len 
                  << " character.Will be set default values." << std::endl;
    }
}

lacksDMA &lacksDMA::operator=(const lacksDMA & other)
{
    Base::operator=(other);
    std::strcpy(color, other.color);
    return *this;
}

void lacksDMA::show() const
{
    std::cout << (const Base &) *this;
    std::cout << "Color: " << color << std::endl;
}


std::ostream &operator<<(std::ostream &os, const lacksDMA & l)
{
    os << (const Base &) l;
    os << "Color: " << l.color;
    return os;
}

hasDMA::hasDMA(const char * s, const char * l, int r)
       : Base(l, r), style(nullptr)
{
    int len = std::strlen(s);
    style = new char[len + 1];
    std::strcpy(style, s);
}

hasDMA::hasDMA(const char * s, const Base &other) : Base(other), style(nullptr)
{
    int len = std::strlen(s);
    style = new char[len + 1];
    std::strcpy(style, s);
}

hasDMA::hasDMA(const hasDMA &other) : Base(other), style(nullptr)
{
    int len = std::strlen(other.style);
    style = new char[len + 1];
    std::strcpy(style, other.style);
}

hasDMA::~hasDMA()
{
    delete[] style;
}

hasDMA &hasDMA::operator=(const hasDMA &other)
{
    delete[] style;
    Base::operator=(other);
    int len = std::strlen(other.style);
    style = new char[len + 1];
    std::strcpy(style, other.style);
    return *this;
}

void hasDMA::show() const
{
    std::cout << (const Base &) *this;
    std::cout << "Style: " << style << std::endl;
}

std::ostream &operator<<(std::ostream &os, const hasDMA &hs)
{
    os << (const Base &) hs;
    os << "Style: " << hs.style << std::endl;
    return os;
}


