#include <iostream>
#include <cstring>

using namespace std;

void clear(char * array, const unsigned int size) {
    for (unsigned int i = 0; i < size; ++i) {
	array[i] = '\0';
    }
}

int main()
{ 
    const unsigned short size = 50;
    char array[size];
    unsigned int sum{}, i{};
    cout << "Enter words (to stop, type the word done):\n";

   do {
	cin.get(array[i]);
	if (array[i] == '\r' || array[i] == char(32) || array[i] == '\n') {
		++sum;
		i = 0;
		clear(array, size);
	} else {
	    ++i;
        }
   } while (strcmp(array, "done") != 0);

   cout << "You entered a total of " << sum << " words." << endl;
}
