#include "emp.h"

/*
  * 1) The operation= don't redefinined because classes made automatic operation
  * and all data correct copuing 
  * 2) The methods ShowAll() and SetAll() virtual because we redefened this methods because 
  * we want print/set data inside a child classes
  * 3)The abstr_emp is virtual base classes we used because we inherit two child classes. And destructor must be virtual for correct deleting data
  * 4) We use a data from two base classes
  * 5) Because we only want print fullname and lastname and this data containe in all classes
  * 6) We get error because we cannot create a virtual base class
*/

abstr_emp::abstr_emp() : fname(""), lname(""), job("") {}
abstr_emp::abstr_emp(const std::string &fn, const std::string &ln, const std::string &j)
	      : fname(fn), lname(ln), job(j) {}

void abstr_emp::ShowAll() const
{
    std::cout << "Fullname: " << fname << ", lastname: " << lname << ", job: " << job << std::endl;
}

void abstr_emp::SetAll()
{
    std::cout << "Set fullname: ";
    std::getline(std::cin, fname);
    std::cout << "Set lastname: ";
    std::getline(std::cin, lname);
    std::cout << "Set job: ";
    std::getline(std::cin, job);
}

abstr_emp::~abstr_emp() {}

std::ostream &operator<<(std::ostream &os, const abstr_emp &e)
{
    os << "Fullname: " << e.fname << ", lastname: " << e.lname 
       << ", job: " << e.job;
    return os;
}

employee::employee() : abstr_emp() {}
employee::employee(const std::string &fn, const std::string &ln, const std::string &j)
         : abstr_emp(fn, ln, j) {}

void employee::ShowAll() const
{
    abstr_emp::ShowAll();
}

void employee::SetAll()
{
    abstr_emp::SetAll();
}

manager::manager() : abstr_emp(), inchargeof(0) {}
manager::manager(const std::string &fn, const std::string &ln, const std::string &j, int ico)
        : abstr_emp(fn, ln, j), inchargeof(ico) {}
manager::manager(const manager &m) 
        : abstr_emp(*this), inchargeof(m.inchargeof) {}
manager::manager(const abstr_emp &e, int ico)
        : abstr_emp(e), inchargeof(ico) {}
void manager::ShowAll() const
{
    abstr_emp::ShowAll();
    std::cout << "Number of charge: " << inchargeof << '\n';
}

void manager::SetAll()
{
    abstr_emp::SetAll();
    std::cout << "Enter number of charge: ";
    std::cin  >> inchargeof;
}

int manager::InChargeOf() const
{
    return inchargeof;
}

int &manager::InChargeOf()
{
    return inchargeof;
}

const std::string fink::ReportsTo() const
{
    return reportsto;
}

std::string &fink::ReportsTo()
{
    return reportsto;
}

fink::fink() : abstr_emp(), reportsto("") {}
fink::fink(const std::string &fn, const std::string &ln, const std::string &j, const std::string &rpo)
     : abstr_emp(fn, ln, j), reportsto(rpo) {}
fink::fink(const abstr_emp &e, const std::string &rpo)
     : abstr_emp(e), reportsto(rpo) {}
fink::fink(const fink &e) : abstr_emp(*this), reportsto(e.reportsto) {}

void fink::ShowAll() const
{
    abstr_emp::ShowAll();
    std::cout << "Report to: " << reportsto << '\n';
}

void fink::SetAll()
{
    abstr_emp::SetAll();
    std::cout << "Enter who reports: ";
    std::cin  >> reportsto;
}

highfink::highfink()
         : abstr_emp(), manager(*this, 0), fink(*this, "") {}

highfink::highfink(const std::string &fn, const std::string &ln, const std::string &j, 
                   const std::string &rpo, int ico)  
         : abstr_emp(fn, ln, j), manager(*this, ico), fink(*this, rpo) {}
highfink::highfink(const abstr_emp &e, const std::string &rpo, int ico) 
         : abstr_emp(e), manager(*this, ico), fink(*this, rpo) {}
highfink::highfink(const fink &f, int ico)
         : abstr_emp(f), fink(f), manager(*this, ico) {}
highfink::highfink(const manager &m, const std::string &rpo)
         : abstr_emp(m), manager(m), fink(*this, rpo) {}
highfink::highfink(const highfink & other)
         : abstr_emp(other), manager(other), fink(other) {}

void highfink::ShowAll() const
{
    abstr_emp::ShowAll();
    std::cout << "Number of charge: " << InChargeOf() 
              << "\nReport to: " << ReportsTo() << '\n';
}

void highfink::SetAll() 
{
    abstr_emp::SetAll();
    std::cout << "Enter number of charge: ";
    std::cin >> InChargeOf();
    std::cout << "Enter who reports: ";
    std::cin  >> ReportsTo();
}

