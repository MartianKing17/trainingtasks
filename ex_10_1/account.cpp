#include "account.h"
#include <iostream>

Account::Account(std::string name, std::string accountNumber, double balance)
	:m_name(name), m_accountNumber(accountNumber), m_balance(balance) {};

void Account::show() const
{
    std::cout << "Name: " << m_name << "\nAccount number: " << m_accountNumber
              << "\nBalance: " << m_balance << std::endl;
}

void Account::addMoney(double income)
{
    if (income <= 0) {
        std::cout << "You cannot input negative value" << std::endl;
    } else {
        m_balance += income;
    }
}

void Account::withdraw(double pullOff)
{
    if (m_balance - pullOff < 0) {
        std::cout << "You cannot withdraw money because the amount is more than on the balance sheet" 
                  << std::endl; 
    } else if (pullOff <= 0) {
        std::cout << "You cannot input negative value" << std::endl; 
    } else {
        m_balance -= pullOff;
    }
}
