#include <string>

class Account 
{
public:
    Account(std::string name, std::string accountNumber, double balance);
    void show() const;
    void addMoney(double income);
    void withdraw(double pullOff);
private:
    std::string m_name;
    std::string m_accountNumber;
    double m_balance;
};

