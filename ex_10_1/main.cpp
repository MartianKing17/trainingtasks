#include "account.h"


int main()
{
    Account maximAccount("Maxim Kovalevskyi", "10993552", 2000);
    maximAccount.show();
    maximAccount.addMoney(500);
    maximAccount.addMoney(1000);
    maximAccount.show();
    maximAccount.withdraw(1200);
    maximAccount.show();
}
