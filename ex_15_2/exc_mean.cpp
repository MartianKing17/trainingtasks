#include "exc_mean.h"
#include <string>

bad_hmean::bad_hmean() : logic_error(""), msg("hmean(): invalid arguments: a = -b") {}
const char * bad_hmean::what() const noexcept
{
    return msg.c_str();
}

bad_gmean::bad_gmean() : logic_error(""), msg("gmean() arguments should be >= 0") {}
const char * bad_gmean::what() const noexcept
{
    return msg.c_str(); 
}


