#pragma once
#include <stdexcept>
#include <string>

class bad_hmean : public std::logic_error
{
public:
    bad_hmean();
    const char * what() const noexcept override;
private:
    std::string msg;
};

class bad_gmean : public std::logic_error
{
public:
    bad_gmean();
    const char * what() const noexcept override;
private:
    std::string msg;
};
