#include <iostream>

using namespace std;
/*
    * The method show() was redefined because we must output 
    * other data which created in child class
    * The operator +=, -= and method bottleCount don't redefined 
    * because we only works with data in base class
    * The operator << and = we don't defined 
    * which virtual because classes we works with only classes data
*/


class Port
{
public:
    Port(const char * br = "none", const char * st = "none", int b = 0);
    Port(const Port &p);
    virtual ~Port();
    Port &operator=(const Port &p);
    Port &operator+=(int b);
    Port &operator-=(int b);
    int bottleCount() const;
    virtual void show() const;
    friend ostream &operator<<(ostream &os, const Port &p);
private:
    char * brand;
    char style[20];
    int bottles;
};

class VintagePort: public Port
{
public:
    VintagePort();
    VintagePort(const char * br, int b, const char * nn, int y);
    VintagePort(const VintagePort &vp);
    ~VintagePort();
    VintagePort &operator=(const VintagePort &vp);
    void show() const;
    friend ostream &operator<<(ostream &os, const VintagePort &vp);
private:
    char * nickname;
    int year;
};
