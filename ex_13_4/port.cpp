#include "port.h"
#include <cstring>

Port::Port(const char * br = "none", const char * st = "none", int b = 0)
      : brand(nullptr), style("none"), bottles(b)
{
    int len = std::strlen(br);
    brand = new char[len + 1]{};
    std::strcpy(brand, br);
    len = std::strlen(st);

    if (len < 20) {
        std::strcpy(style, st);
    } else {
        std::cout << "You try write in char array more 20 character. Set dafault" << std::endl;
    }
}

Port::Port(const Port &p) : brand(nullptr), style("none"), bottles(p.bottles)
{
    brand = new char[std::strlen(p.brand) + 1]{};
    std::strcpy(brand, p.brand);
    std::strcpy(style, p.style);
}

Port::~Port()
{
    delete[] brand;
}

Port &Port::operator=(const Port &p)  
{
    delete[] brand;
    brand = new char[std::strlen(p.brand) + 1]{};
    std::strcpy(brand, p.brand);
    std::strcpy(style, p.style);
    bottles = p.bottles;
    return *this;
}

Port &Port::operator+=(int b)
{
    return Port(brand, style, bottles + b);
}

Port &Port::operator-=(int b)
{
    if (b > bottles) {
        std::cout << "You cannot minus more bottle you have" << std::endl;
        return *this;
    }   
    return Port(brand, style, bottles - b);
}

int Port::bottleCount() const
{
    return bottles;
}

void Port::show() const 
{
    std::cout << brand << ", " << style << ", " << bottles;
}

ostream &operator<<(ostream &os, const Port &p)
{
    os << brand << ", " << style << ", " << bottles;
    return os;
}

VintagePort::VintagePort() : Port(), nickname(nullptr), year(0)
{
    nickname = new char[10]{};
}

VintagePort::VintagePort(const char * br, int b, const char * nn, int y)
            : Port(br, b), nickname(nullptr), year(y)
{
    nickname = new char[std::strlen(nn) + 1];
    std::strcpy(nickname, nn);
}

VintagePort::VintagePort(const VintagePort &vp)
            : Port(vp), nickname(nullptr), year(vp.year)
{
    nickname = new char[std::strlen(vp.nickname) + 1];
    std::strcpy(nickname, vp.nickname);
}

VintagePort::~VintagePort()
{
    delete[] nickname;
}

VintagePort &VintagePort::operator=(const VintagePort &vp)
{
    Port::operator=(vp);
    year = vp.year;
    delete[] nickname;
    nickname = new char[std::strlen(vp.nickname) + 1];
    std::strcpy(nickname, vp.nickname);
    return *this;
}

void VintagePort::show() const
{
    Port::show();
    std::cout << ", " << nickname << ", " << year;
}

ostream &operator<<(ostream &os, const VintagePort &vp)
{
    os << (const Port &)vp;
    os << ", " << nickname << ", " << year;
    return os;
}



