#include <iostream>
#include <string>

using namespace std;

struct Pizza
{
   string namePizzaCompany;
   int diameter;
   double weight;
};

void enterPizzaData(Pizza &pizza)
{
  cout << "Input the name of pizza company: ";
  getline(cin, pizza.namePizzaCompany);
  cout << "Input the diameter of pizza: ";
  cin >> pizza.diameter;
  cout << "Input the pizza weight: ";
  cin >> pizza.weight;
}

void printData(Pizza pizza)
{
  cout << "Name of pizza company: " << pizza.namePizzaCompany
       << "\nThe diameter of pizza: " << pizza.diameter
       << "\nThe pizza weight: " << pizza.weight << endl;
}

int main()
{
  int sizeOfPizzaArray;
  cout << "Enter size of data: ";
  cin >> sizeOfPizzaArray;
  Pizza pizzas[sizeOfPizzaArray];

  for (Pizza &pizza : pizzas) {
	cin.get();
  	enterPizzaData(pizza);
  }
  cout << endl;
  
  for (Pizza pizza : pizzas) {
  	printData(pizza);
  }
  return 0;
}
