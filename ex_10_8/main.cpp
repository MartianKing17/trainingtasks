#include "list.h"
#include <iostream>

using namespace std;

int main()
{
    int number{}, index{};
    string command{};
    unsigned int size{};
    cout << "Enter size of list: ";
    cin  >> size;
    
    if (size <= 0) {
        cout << "Error. Not correct size" << endl;
        return 0;
    }
    
    List<int> list(size);
    cout << "Enter command: push, pop, change, or q(for quit): ";
    cin.get();
    while (getline(cin, command) && command != "q") {
        if (command == "push") {
            cout << "Enter integer number: ";
            cin  >> number;
            cin.get();
            if (list.isFull()) {
                cout << "You cannot add new value. Because list is full" << endl;
            } else {
                list.push(number);
            }
        } else if (command == "pop") {
            number = list.pop();
            cout << "Poped number: " << number << endl;
        } else if (command == "change") {
            cout << "Enter index: ";
            cin  >> index;
            cout << "Enter new number: ";
            cin  >> number;
            cin.get();
            list.at(index) = number;
        }
        cout << "Enter command: push, pop, change, or q(for quit): ";
        command.clear();
    }

}
