#pragma once
#include <iostream>

template <typename T>
class List
{
 public:
    List(unsigned int size = 20);
    void push(const T &value);
    T pop();
    bool isEmpty();
    bool isFull();
    T & at(unsigned int index);
    ~List();
 private:
    struct Node
    {
        T data;
        Node * prev;
        Node * next;
    };
    
    unsigned int m_size;
    unsigned int m_count;
    Node * m_head;
    Node * m_tail;
};

template <typename T>
List<T>::List(unsigned int size)
        : m_size(size), m_head(nullptr), m_tail(nullptr), m_count(0) {};

template <typename T>
void List<T>::push(const T &value)
{
    if (m_head == nullptr) {
        m_head = new Node{value, nullptr, m_tail};
	m_tail = new Node {0, m_head, nullptr};
    } else {

        if (isFull()) {
            return;
        }

        Node * next = m_tail, *prev;
        Node * item = new Node{value};
        item->next = next;        
        item->prev = next->prev;
        next->prev = item;
        next = nullptr;
        prev = item->prev;
        prev->next = item;
        prev = nullptr;
        item = nullptr;
    }

    ++m_count;
}

template <typename T>
T List<T>::pop()
{
    T data{};

    if (m_count <= 0) {
        return {};
    }

    if (m_head->next == m_tail) {
        data = m_head->data;
        delete[] m_head;
        m_head = nullptr;
    } else {
        Node * next = m_tail;
        Node * item = m_tail->prev;
        Node * prev = item->prev;
        prev->next = next;
        next->prev = prev;
        item->prev = nullptr;
        item->next = nullptr;
        prev = nullptr;
        next = nullptr;
        data = item->data;
        delete[] item;
        item = nullptr;
    }
    --m_count;
    return data;
}

template <typename T>
bool List<T>::isEmpty()
{
    return m_count == 0;
}

template <typename T>
bool List<T>::isFull()
{
    return m_count >= m_size;
}

template <typename T>
T & List<T>::at(unsigned int index)
{

    if (index > m_count) {
        return m_head->data;
    }

    Node * current = m_head;
    for (unsigned int i = 1; i <= index; ++i) {
        current = current->next;
    }

    return current->data;
}

template <typename T>
List<T>::~List()
{
    for (unsigned int i = 0; i < m_count; ++i) {
        pop();
    }

    m_head = nullptr;
    m_tail = nullptr;
}  

