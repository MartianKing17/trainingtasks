#include <iostream>
#include <string>

using namespace std;

struct Car {
  int builded;
  string make;

};

int main()
{
    int sizeOfCarArr{};
    cout << "How many cars do you wish to catalog? ";
    cin >> sizeOfCarArr;
    Car * cars = new Car[sizeOfCarArr];

    for (int i = 0; i < sizeOfCarArr; ++i) {
        cout << "Car #" << (i+1) << ":\n";
        cout << "Please enter the make: ";
        cin.get();
        getline(cin, cars[i].make);
   
        if (cars[i].make.size() <= 1) {
           --i;
           cout << "You input short name. Try input again" << endl;
           continue;
        }

        cout << "Please enter the year made: ";
        cin >> cars[i].builded;
    }

    for (int i = 0; i < sizeOfCarArr; ++i) {
       cout << cars[i].builded << " " << cars[i].make << endl;
    }
}
