#include <iostream>

using namespace std;

int main()
{
   int arraySize = 50;
   char firstNameBuffer[arraySize];
   char lastNameBuffer[arraySize];
   int age;
   char grade;
   cout << "What is your first name? ";
   cin.getline(firstNameBuffer, arraySize);
   cout << "What is your last name? ";
   cin.getline(lastNameBuffer, arraySize);
   cout << "What letter grade do you deserve? ";
   cin.get(grade).get();
   cout << "What is your age? ";
   cin >> age;
   cout << "Name: " <<lastNameBuffer << ", " << firstNameBuffer 
        << "\nGrade: " << char(grade + 1) << "\nAge: " << age << std::endl;
}
