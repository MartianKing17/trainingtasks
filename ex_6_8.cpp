#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
  char ch{};
  unsigned int number;
  string filename{};
  cout << "Enter the file name: ";
  cin >> filename;
  ifstream readFile(filename);

  if (!readFile.is_open()) {
     cout << "Some wrong when trying openning file" << endl;
     return 0;
  }

  while (!readFile.eof()) {
    readFile.get(ch); 
    ++number;
  }

  readFile.close();
  cout << "The number of characters in the file: " << number << endl;
}
