#include <iostream>

using namespace std;

void startMenu() {
   cout << "Please enter one of the following choices:\n";
   cout << "c) carnivore";
   cout.width(14);
   cout << "p) pianist" << "\nt) tree ";
   cout.width(14); 
   cout << "g)game" << "\nq) exit" << endl;
}

int main()
{
  char symbol{};
  startMenu();
  
  while (true) {
    cin >> symbol;

    if (symbol == 'q') {
        break;
    }

    switch(symbol) {
        case 'c':
             cout << "The Tiger is a carnivore" << endl;
             break;
        case 'p':
             cout << "Pianist is a man who play on the piano" << endl;
             break;
	case 't':
             cout << "A maple is a tree" << endl;
             break;
	case 'g':
             cout << "A Doom is a videogame" << endl;
             break;
        default:
             cout << "Please enter a: c, p, t, g, or q: ";
             break;
    }
  }
}
