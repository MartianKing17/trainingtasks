#include <iostream>

using namespace std;

int main()
{
    int sum{}, value{};
    
    do {
       cout << "Enter number: ";
       cin >> value;
       sum += value;
    } while(value != 0);
  
    cout << "Sum entered numbers: " << sum << endl;
}
