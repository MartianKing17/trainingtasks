#include <iostream>

using namespace std;

int main()
{
  int income{}, tax{};
  while (true) {
    cout << "Input your income: ";
    cin >> income;
   
    if (!cin || income < 0) {
       cout << "Not correct data" << endl;
       break;
    }

    if (income < 10000) {
       tax = 0;
    } else if (income < 20000) {
       tax = income * 0.1;
    } else if (income <= 35000) {
       tax = income * 0.15;
    } else {
       tax = 5000 * 0.0 + 10000 * 0.1 + 20000 * 0.15;
       income -= 35000;
       tax += income * 0.2;
    }

    cout << "Tax: " << tax << endl;
  } 
}
