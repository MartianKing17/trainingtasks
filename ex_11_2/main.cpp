#include <iostream>
#include <cstdlib>
#include <ctime>
#include "vector.h"

using namespace std;
using VECTOR::Vector;

int main()
{
    unsigned long steps{};    
    double direction{}, target{}, dstep{};
    Vector step{};
    Vector result(0.0, 0.0);
    srand(time(0));
    cout << "Enter target distance (q to quit): ";

    while(cin >> target)
    {
        cout << "Enter step length: ";
        if(!(cin >> dstep)) {
            break;
        }

        while(result.magval() < target) {
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            result = result + step;
            ++steps;
        }

        cout << "After " << steps << " steps, the subject has the following location:\n";
        cout << result << endl;
        result.polar_mode();
        cout << " or\n" << result << endl;
        cout << "Averange outward distance per step = " << result.magval()/steps << endl;
        steps = 0;
        result.reset(0.0, 0.0);
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    cin.clear();
    while (cin.get() != '\n')
        continue;
    return 0;
}
