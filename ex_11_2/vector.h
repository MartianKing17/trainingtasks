#pragma once
#include <iostream>

namespace VECTOR
{
    class Vector
    {
    public: 
        enum Mode {RECT, POL};
    public:
        Vector(); // +
        Vector(double n1, double n2, Mode form = RECT); //+
        void reset(double n1, double n2, Mode form = RECT); 
        double xval() const;
        double yval() const;
        double magval();
        double angval();
        void polar_mode(); // +
        void rect_mode(); // +
        Vector operator+(const Vector &b) const; // +
        Vector operator-(const Vector &b) const; // +
        Vector operator-() const; // +
        Vector operator*(double n) const; // +
        friend Vector operator*(double n, const Vector &a); // +
        friend std::ostream &operator << (std::ostream &os, Vector &v); // +
        ~Vector() = default;
    private:
        double x;
        double y;
        double mag;
        double ang;
        Mode mode;
        void set_mag(); // +
        void set_ang(); // +
        void set_x(); // +
        void set_y(); // +
    };
}
