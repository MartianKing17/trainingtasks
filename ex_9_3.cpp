#include <iostream>
#include <new>
#include <cstring>

using namespace std;

struct chaff
{
    char dross[20];
    int slag;
};


int main()
{
    char *buffer = new char[50];
    chaff * p = new (buffer) chaff[2];
    strcpy(p[0].dross, "Some text"); 
    p[0].slag = 1;
    strcpy(p[1].dross, "Text");
    p[1].slag = 3;

    for (int i = 0; i < 2; ++i) {
        cout << "Dross: " << p[i].dross << "\nSlag: " << p[i].slag << endl;
    }

    for (int i = 0; i < 50; ++i) {
        cout << buffer[i];
    }
}
