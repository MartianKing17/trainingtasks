#include "Plorg.h"
#include <iostream>

using namespace std;

int main()
{
    const short len = 19;
    int CI{};
    char name[len];
    cout << "Enter name for Plorg: " << endl;
    cin.get(name, len);
    Plorg plorg(name);
    cout << "Enter CI: ";    
    cin  >> CI;
    plorg.changeCI(CI);
    plorg.showInfo();
}
