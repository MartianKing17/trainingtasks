

class Plorg
{
 public:
    Plorg(const char * name = "Plorga");
    void showInfo() const;
    void changeCI(int CI);
 private:
    static const unsigned short len = 19;
    char m_name[len];
    int m_CI;
};
