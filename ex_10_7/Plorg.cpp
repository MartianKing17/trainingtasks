#include "Plorg.h"
#include <cstring>
#include <iostream>

Plorg::Plorg(const char *name) : m_CI(50)
{
    if (strlen(name) > len) {
        std::cout << "More than " << len << " character. Will be set default name" << std::endl;
        strcpy(m_name, "Plorga");     
    }
    
    strcpy(m_name, name);
}

void Plorg::showInfo() const
{
    std::cout << "Name: " << m_name << "\nCI: " << m_CI << std::endl;
}

void Plorg::changeCI(int CI)
{
    m_CI = CI;
}
