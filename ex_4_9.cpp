#include <iostream>
#include <string>

using namespace std;

struct CandyBar 
{
  string name;
  double weight;
  int calories;
};

int main()
{
  const int size = 3;

  CandyBar * snacks = new CandyBar[size]{
	{"Mocha Munch", 2.3 , 350},
	{"Roshen", 0.750, 300},
	{"Corona", 0.6, 100}
  };

  for (int i = 0; i < size; ++i)
  {
    cout << "Name: " << snacks[i].name 
       << "\nWeight: " << snacks[i].weight 
       << "\nCalories: " << snacks[i].calories << std::endl;
  }

  delete[] snacks;
  snacks = nullptr;
  return 0;
}
