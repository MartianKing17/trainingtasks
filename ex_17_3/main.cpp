#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char * argv[])
{
    if (argv[1] == 0 || argv[2] == 0) {
        cout << "You don't set's a file names" << endl;
        return 0;
    }

    string data{};
    ifstream fin;
    ofstream fout;

    fin.open(argv[1]);
    fout.open(argv[2]);

    if (!fin.is_open() || !fout.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }

    while (!fin.eof()) {
	getline(fin, data);
	data += '\n';
        fout.write(data.c_str(), data.size());
    }

    fin.close();
    fout.close();
    return 0;
}
