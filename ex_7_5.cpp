#include <iostream>

using namespace std;

unsigned long long factorial(unsigned long long number)
{
    if (number <= 1) {
        return 1;
    }

    return number * factorial(number - 1);
}


int main()
{
   unsigned long long number{};
   cout << "Enter number for calculate factorial(if you want exit, enter a alphabet): ";

   while ((cin >> number)) {
        cout << number << "! = " << factorial(number) << '\n';
        cout << "Enter number for calculate factorial(if you want exit, enter q): ";
   }
}
