#include <iostream>
#include <array>

using namespace std;

int main()
{
    const int size = 101;
    array<long double, size> factorial;
    factorial[0] = factorial[1] = 1.0;
    
    for(int i = 2; i < size; ++i)
 	factorial[i] = i * factorial[i-1];

    for (int i = 0; i < size; ++i)
    	cout << i <<"! = " << factorial[i] << endl;
}
