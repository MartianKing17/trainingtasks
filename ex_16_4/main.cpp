#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

int reduce(long ar[], int n);

int main()
{
    int len = 20;
    long array[] =
    {
        1, 23, 12, 15, 16, 13, 14, 14, 14, 9, 11, 57, 34, 25, 86, 45, 23, 97, 99, 100
    };

    cout << "Original:\n";
    for(int i = 0; i < len; ++i) {
        cout << array[i] << ' ';
    }

    cout << endl;
    len = reduce(array, len);    
    cout << "Change original in reduce:\n";

    for(int i = 0; i < 20; ++i) {
        cout << array[i] << ' ';
    }
    
    cout << endl;
    return 0;
}

bool equalTo(int i, int j)
{
    return i == j;
}

int reduce(long ar[], int n)
{
    sort(ar, ar + n);
    auto it = unique_copy(ar, ar + n, ar);
    it = unique_copy(ar, it, ar, equalTo);
    return distance(ar, it);
}
