#include "stonewt.h"
#include <iostream>

Stonewt::Stonewt() : m_stone(0), m_lbs(0), m_stage(0) {}

Stonewt::Stonewt(double stone, double lbs) : m_stone(stone), m_lbs(lbs), m_stage(0) {}

void Stonewt::stage(int s)
{
    m_stage = s;
}

bool Stonewt::operator==(const Stonewt &s)
{
    return m_stone == s.m_stone;
}

bool Stonewt::operator!=(const Stonewt &s)
{
    return m_stone != s.m_stone;
}

bool Stonewt::operator<(const Stonewt &s)
{
    return m_stone < s.m_stone;
}

bool Stonewt::operator>(const Stonewt &s)
{
    return m_stone > s.m_stone;
}

bool Stonewt::operator<=(const Stonewt &s)
{
    return m_stone <= s.m_stone;
}

bool Stonewt::operator>=(const Stonewt &s)
{
    return m_stone >= s.m_stone;
}

bool Stonewt::operator==(double num)
{
    return m_stone == num;
}

bool Stonewt::operator!=(double num)
{
    return m_stone != num;
}

bool Stonewt::operator<(double num)
{
    return m_stone < num;
}

bool Stonewt::operator>(double num)
{
    return m_stone > num;
}

bool Stonewt::operator<=(double num)
{
    return m_stone <= num;
}

bool Stonewt::operator>=(double num)
{
    return m_stone >= num;
}

std::ostream &operator<<(std::ostream &os, const Stonewt &s)
{
    double pounds = s.m_stone * s.lbs_per_stn + s.m_lbs;
    switch(s.m_stage) {
        case 0:
            os << s.m_stone << " stone, " << s.m_lbs << " pounds";
            break;
        case 1:
            os << int(pounds) << " pounds";
            break;
        case 2:
            os << pounds << " pounds";
            break;
	    default:
	        break;
    }

    return os;
}

std::istream &operator>>(std::istream &is, Stonewt &s)
{

    std::cout << "Enter your stones: ";
    is >> s.m_stone;
    std::cout << "Enter your pds: ";
    is >> s.m_lbs;
    s.m_stage = 0; 
    return is;
}

bool operator==(double num, Stonewt &s)
{
    return s == num;
}

bool operator!=(double num, Stonewt &s)
{
    return s != num;
}

bool operator<(double num, Stonewt &s)
{
    return s < num;
}

bool operator>(double num, Stonewt &s)
{
    return s > num;
}

bool operator<=(double num, Stonewt &s)
{
    return s <= num;
}

bool operator>=(double num, Stonewt &s)
{
    return s >= num;
}

Stonewt::operator double()
{
    return m_stone;
}


