#pragma once
#include <iostream>

class Stonewt
{
public:
    Stonewt(double stone, double lbs);
    Stonewt();
    void stage(int s = 0);
    ~Stonewt() = default;
    bool operator==(const Stonewt &s);
    bool operator!=(const Stonewt &s);
    bool operator<(const Stonewt &s);
    bool operator>(const Stonewt &s);
    bool operator<=(const Stonewt &s);
    bool operator>=(const Stonewt &s);
    bool operator==(double num);
    bool operator!=(double num);
    bool operator<(double num);
    bool operator>(double num);
    bool operator<=(double num);
    bool operator>=(double num);
    friend std::ostream &operator<<(std::ostream & os, const Stonewt &s);
    friend std::istream &operator>>(std::istream & is, Stonewt &s);
    friend bool operator==(double num, Stonewt &s);
    friend bool operator!=(double num, Stonewt &s);
    friend bool operator<(double num, Stonewt &s);
    friend bool operator>(double num, Stonewt &s);
    friend bool operator<=(double num, Stonewt &s);
    friend bool operator>=(double num, Stonewt &s);
    explicit operator double();
private:
    static const int lbs_per_stn = 14;
    double m_stone;
    double m_lbs;
    int m_stage;
};
