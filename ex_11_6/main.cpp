#include "stonewt.h"
#include <iostream>

using namespace std;

double min(Stonewt * stonewts, const short size)
{
    if (size <= 0) {
        cout << "Uncorrecting size" << endl;
        return 0;
    }

    double minimum = double(stonewts[0]);
    
    for (short i = 1; i < size; ++i) {
       if (minimum > stonewts[i]) {
            minimum = double(stonewts[i]);
        }
    }

    return minimum;
}

double max(Stonewt * stonewts, const short size)
{
    if (size <= 0) {
        cout << "Uncorrecting size" << endl;
        return 0;
    }

    double maximum = double(stonewts[0]);
    
    for (short i = 1; i < size; ++i) {
       if (maximum < stonewts[i]) {
            maximum = double(stonewts[i]);
        }
    }

    return maximum;
}


int main()
{
    const short size = 6;
    Stonewt stonewts[size] = {{15., 0.}, {11., 0.}, {12., 0.}};

    for (short i = 3; i < size; ++i) {
        cin >> stonewts[i];
    }

    cout << "Minimum: " << min(stonewts, size)
         << ", maximum: " << max(stonewts, size) << endl;

    for(short i = 0; i < size; ++i) {
        if (stonewts[i] >= stonewts[1]) {
            cout << "The " << stonewts[i] << " bigger than or equal to " << stonewts[1] << endl;
        }
    }
}
