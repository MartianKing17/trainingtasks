#include <iostream>
#include <string>

using namespace std;

struct CandyBar 
{
  string name;
  double weight;
  int calories;
};

int main()
{
  CandyBar snack {"Mocha Munch", 2.3 , 350};
  cout << "Name: " << snack.name 
       << "\nWeight: " << snack.weight 
       << "\nCalories: " << snack.calories << std::endl;
  return 0;
}
