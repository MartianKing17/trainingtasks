#include <iostream>

using namespace std;


template <typename T>
long double sum_value(T value) 
{
    return static_cast<long double>(value);
}

template <typename T, typename ...Args>
long double sum_value(T value, Args... args)
{
    return sum_value(args...) + value;
}

int main()
{
    cout << "Was set in paraments: 4, 3.12, 0.5, 16'000'000, -12'000.5253" << endl;
    cout << "Result: " << sum_value(4, 3.12, 0.5, 16'000'000, -12'000.5253) << endl;
}
