#include "golf.h"
#include <iostream>

using namespace std;

int main()
{
    int arrSizeGolfPlayers{};
    cout << "Enter the number of gamers: ";
    cin  >> arrSizeGolfPlayers;
    golf * gamers = new golf[arrSizeGolfPlayers]{};

    for (int i = 0; i < arrSizeGolfPlayers; ++i) {
        if (!setgolf(gamers[i])) {
            cout << "Error. You enter a empty line" << endl;
            return 0;
        }
    }

    cout << "Data of gamers: " << endl; 
    for (int i = 0; i < arrSizeGolfPlayers; ++i) {
        showgolf(gamers[i]);
    }
}

