#include "golf.h"
#include <iostream>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;

void setgolf(golf & g, const char * name , int hc)
{
    if (strlen(name) >= Len) {
        cout << "Character limit exceeded. Maximum: 30. Input short name" << endl;
        return;
    }
    strcpy(g.fullname, name);
    g.handicap = hc;
}

int setgolf(golf & g)
{
    cin.get();
    cout << "Input your name: ";
    cin.getline(g.fullname, Len);
    
    if (!cin) {
        cin.clear();
        while (cin.get() != '\n') {
            continue;
        }
        return 0;
    }

    cout << "Enter the handicap: ";
    cin  >> g.handicap;
    if (g.fullname == "") {
        return 0;
    }
    
    return 1;
}

void handicap(golf & g, int hc)
{
    g.handicap = hc;
}

void showgolf(const golf & g)
{
    cout << "Fullname: " << g.fullname << "\nHandicap: " << g.handicap << endl;
}

