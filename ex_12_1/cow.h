#pragma once

class Cow
{
public:
    Cow();
    Cow(const char * nm, const char * ho, double wt);
    Cow(const Cow &c);
    ~Cow();
    Cow &operator=(const Cow &c);
    void showCow() const;
private:
    char m_name[20];
    char * m_hobby;    
    double m_weight;
};
