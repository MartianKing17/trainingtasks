#include "cow.h"
#include <iostream>
#include <cstring>

using namespace std;

Cow initCow()
{
    char name[20];
    int weight;
    char hobby[512];
    cout << "Enter the cow name: ";
    cin.getline(name, sizeof name);
    
    if(!cin) {
        cout << "Error. You cannot enter name the summary more 20 character. " 
             << "Will be set default name" << endl;
        strcpy(name, "");  
        cin.clear();
        while(cin.get() != '\n') {
            continue;
        }
    }

    cout << "Enter the cow hobby: ";
    cin.getline(hobby, sizeof hobby);
    
    if(!cin) {
        cout << "Error. You cannot enter name the summary more 512 character. " 
             << "Will be set default hobby" << endl;
        strcpy(hobby, "");        
        cin.clear();
        while(cin.get() != '\n') {
            continue;
        }
    }

    cout << "Enter the cow weight: ";
    cin  >> weight;
    if(!cin) {
        cout << "Error. You enter not digits. Will be set default weight" << endl;
        weight = 100;        
        cin.clear();
        while(cin.get() != '\n') {
            continue;
        }
    }
    
    return Cow(name, hobby, weight);
}

int main()
{
    Cow cow;
    cow = initCow();
    cow.showCow();
}

