#include "cow.h"
#include <iostream>
#include <cstring>

Cow::Cow(): m_name(""), m_hobby(nullptr), m_weight(100)
{
    m_hobby = new char[10]{}; 
}

Cow::Cow(const char * nm, const char * ho, double wt)
{
    if (strlen(nm) <= sizeof m_name) {
        strcpy(m_name, nm);
    } else {
        strcpy(m_name, "");
    }

    m_hobby = new char[strlen(ho) + 1]{};
    strcpy(m_hobby, ho);
    m_weight = wt;
}

Cow::Cow(const Cow &c)
{
    m_hobby = new char[strlen(c.m_hobby) + 1]{};
    strcpy(m_hobby, c.m_hobby);
    strcpy(m_name, c.m_name);
    m_weight = c.m_weight;
}

Cow::~Cow()
{
    delete[] m_hobby;
    m_hobby = nullptr;
}

Cow &Cow::operator=(const Cow &c)
{
    delete[] m_hobby;
    m_hobby = new char[strlen(c.m_hobby) + 1]{};
    strcpy(m_hobby, c.m_hobby);
    strcpy(m_name, c.m_name);
    m_weight = c.m_weight;
    return *this;
}

void Cow::showCow() const
{
    std::cout << "Name: " << m_name << "\nHobby: " << m_hobby << "\nWeight: " << m_weight << std::endl; 
}
