#pragma once

class Customer
{
public:
    Customer();
    void set(long when);
    long when() const;
    int ptime() const;
private:
    long arrive;
    int processtime; 
};


