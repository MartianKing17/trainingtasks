#include "customer.h"
#include <cstdlib>

Customer::Customer()
{
    arrive = processtime = 0;
}

void Customer::set(long when) 
{
    processtime = std::rand() % 3 + 1;
    arrive = when;
}

long Customer::when() const
{
    return arrive;
}

int Customer::ptime() const
{
    return processtime;
}

