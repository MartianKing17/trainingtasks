#include <iostream>

using namespace std;

void fill(double * expenses, const char * *(snames), const int seasons)
{
    for (int i = 0; i < seasons; i++) {
        cout << "Enter " << snames[i] << " expenses: ";
        cin >> expenses[i];
    }
}

void show(double * expenses, const char * *(snames), const int seasons)
{
    double total = 0.0;
    cout << "\nEXPENSES\n";
    for (int i = 0; i < seasons; i++) {
    cout << snames[i] << ": $" << expenses[i] << endl;
    total += expenses[i];
    }
    cout << "Total Expenses: $" << total << endl;
}

int main()
{
    const int seasons = 4;
    const char *(snames)[seasons] = {"Spring", "Summer", "Fall", "Winter"};
    double expenses[seasons]{};
    fill(expenses, snames, seasons);
    show(expenses, snames, seasons);
    return 0;
}


