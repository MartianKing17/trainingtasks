#include <iostream>
#include <string>
#include <vector>
#include <array>

using namespace std;

bool isVowels(char ch)
{
  bool isIt{};
  array<char,5> vowels = {'a', 'e', 'i', 'o', 'u'};
  
  for (char vowel : vowels) {
      if (vowel == ch) {
        isIt = true;
        break;
      }
  }

  return isIt;
}

int main()
{
    unsigned int beginVowels{}, beginConsonants{}, other{};
    char ch{};
    vector<string> words{};
    string word{};
    cout << "Enter words (q to quit): ";
    
    while (true) {
      cin.get(ch);

      if (ch == '\n' || ch == ' ' || ch == '\r') {

         if (word == "q") {
             break;
         } else {
	     words.push_back(word);
	     word.clear();
         }
      } else {
         word += ch;
      }
    }
   
    for (string word : words) {
      if (isalpha(word[0])) {
         if (isVowels(word[0])) {
             ++beginVowels;
         } else {
             ++beginConsonants;
        }
      } else {
          ++other;
      }
    }

    cout << beginVowels << " words beginning with vowels\n" << beginConsonants 
         << " words beginning with consonants\n" << other << " others" << endl; 
}
