#include <iostream>
#include <string>

using namespace std;

struct Contribution{
  string name;
  double amount;
};

int main()
{
   string donorsWhoDonateMoreMoney{}, otherDonors;
   unsigned int numberOfContributors{};
   cout << "How many contributors you want enter: ";
   cin >> numberOfContributors;
   Contribution * contributors = new Contribution[numberOfContributors];

   for (unsigned int i = 0; i < numberOfContributors; ++i) {
      cout << "What name #" << i + 1 << " contributor: ";
      cin >> contributors[i].name;
      cout << "How many " << contributors[i].name << " investing: ";
      cin >> contributors[i].amount;
   }

   for (unsigned int i = 0; i < numberOfContributors; ++i) {
        if (contributors[i].amount >= 10000) {
		donorsWhoDonateMoreMoney += "Name: " + contributors[i].name 
                                         + ", donate: " + to_string(contributors[i].amount) + '\n';
        } else if (contributors[i].amount > 0 && contributors[i].amount < 10000) {
		otherDonors += "Name: " + contributors[i].name 
                                         + ", donate: " + to_string(contributors[i].amount) + '\n';
        }
   }

   if (!donorsWhoDonateMoreMoney.empty()) {
       cout << "Grand Patrons:\n" << donorsWhoDonateMoreMoney;
     
   }

   if (!otherDonors.empty()) {
       cout << "Patrons:\n" << otherDonors;
   }
   
   if (donorsWhoDonateMoreMoney.empty() || otherDonors.empty()) {
       cout << "none" << endl;
   }
   
}
