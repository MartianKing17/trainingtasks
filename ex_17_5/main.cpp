#include <string>
#include <list>
#include <set>
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace std;

void setFriendName(ifstream & in, list<string> &friendList);
template <typename T>
void display(const T &friendList, const string &text);

int main()
{
    list<string> mattFriend, patFriend, friendList;
    ifstream fin;
    fin.open("mat.dat");
    if (!fin.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }
	
    setFriendName(fin, mattFriend);
    fin.close();
    fin.clear();
    mattFriend.sort();
    display(mattFriend, "Matt friends: ");
    cout << endl;

    fin.open("pat.dat");
    if (!fin.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }
	
    setFriendName(fin, patFriend);
    fin.close();
    fin.clear();
    patFriend.sort();
    display(patFriend, "Pat friends: "); 
    cout << endl;


    friendList.merge(mattFriend);
    friendList.merge(patFriend);
    set<string> friends(friendList.begin(), friendList.end());

    ofstream fout("matnpat.dat");
    if (!fout.is_open()) {
        cout << "Some wrong" << endl;
        return 0;
    }

    for (const string &data : friends) {
        fout.write(data.c_str(), data.size());
    }

    fout.close();
    return 0;
}

void setFriendName(ifstream & in, list<string> &friendList)
{
    string data{};
    
    while (!in.eof()) {
        getline(in, data);
        data += '\n';
        friendList.push_back(data);
        data.clear();
    }
}

template <typename T>
void display(const T &friendList, const string &text)
{
    cout << text;
    for (const string &name : friendList) {
        cout << name;
    }       
}
