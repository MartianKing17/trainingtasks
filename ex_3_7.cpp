#include <iostream>

using namespace std;

int main()
{
    const double gallonsToLitres = 3.875, miToKm = 1.609;
    double mpg, litres{}, gallons{};
    std::cout << "Miles per gallons: ";
    std::cin >> mpg;
    litres = 100 / (mpg * miToKm) * gallonsToLitres;
    std::cout << mpg << " mpg is about " << litres << "/100 km" << std::endl;
}

