#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include "vector.h"

using namespace std;
using VECTOR::Vector;

int main()
{
    unsigned long steps{};    
    double direction{}, target{}, dstep{};
    Vector step{};
    Vector result(0.0, 0.0);
    srand(time(0));
    ofstream writeFile("data.txt");

    if (!writeFile.is_open()) {
        cout << "File wasn't opened" << endl;
    }
    cout << "Enter target distance (q to quit): ";

    while(cin >> target)
    {
        cout << "Enter step length: ";
        if(!(cin >> dstep)) {
            break;
        }
        writeFile << "Target Distance: " << target << ", Step Size: " << dstep << endl;

        while(result.magval() < target) {
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            result = result + step;
            writeFile << steps << ": " << result << endl;
            ++steps;
        }

        writeFile << "After " << steps << " steps, the subject has the following location:\n";
        writeFile << result << endl;

        cout << "After " << steps << " steps, the subject has the following location:\n";
        cout << result << endl;
        result.polar_mode();
        writeFile << " or\n" << result << endl;
        writeFile << "Averange outward distance per step = " << result.magval()/steps << endl;
        cout << " or\n" << result << endl;
        cout << "Averange outward distance per step = " << result.magval()/steps << endl;
        steps = 0;
        result.reset(0.0, 0.0);
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    cin.clear();
    while (cin.get() != '\n')
        continue;
    return 0;
}
