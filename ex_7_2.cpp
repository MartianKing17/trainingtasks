#include <iostream>
#include <numeric>
#include <algorithm>

using namespace std;

void input(unsigned int * arr, const unsigned short arrSize)
{
    for (unsigned short i = 0; i < arrSize; ++i) {
        cout << "Input the #" << (i + 1) << " golf score: ";
        cin >> arr[i]; 
    }
}

int calcAverange(unsigned int * arr, const unsigned short arrSize)
{
    return accumulate(arr, arr + arrSize, 0);
}

void display(unsigned int * arr, const unsigned short arrSize, int averange)
{
   for_each(arr, arr + arrSize, [](unsigned int data) { cout << data << " ";});
   cout << "\nAverange: " << averange << endl;
}


int main()
{
   const unsigned short arrSize = 10;
   unsigned int arr[arrSize]{}, averange{}; 
   input(arr, arrSize);
   averange = calcAverange(arr, arrSize);
   display(arr, arrSize, averange);
}
