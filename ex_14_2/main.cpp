#include "wine.h"
#include <iostream>

using namespace std;

int main()
{
    int years{};
    char lab[50]{};
    const int arrSize = 3;
    int y[arrSize] = {1993, 1995, 1998};
    int b[arrSize] = {48, 60, 72};

    cout << "Enter name of wine: ";
    cin.getline(lab, 50);
    cout << "Enter number of years: ";
    cin >> years;
    Wine holding(lab, years);
    holding.getBottles();
    holding.show();
    Wine more("Gushing Grape Red", arrSize, y, b);
    more.show();
    cout << "Total bottles for " << more.label() << ": " << more.sum() << endl;
    cout << "Bye\n";
    return 0;
}
