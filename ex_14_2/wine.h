#include <string>
#include <utility>
#include <valarray>

class Wine: std::string, std::pair<std::valarray<int>, std::valarray<int>>
{
public:
    Wine();
    Wine(const char * label, int year, const int years[], const int bottles[]);
    Wine(const char * label, int year);
    void getBottles();
    std::string label() const;
    void show() const;
    int sum();
};
