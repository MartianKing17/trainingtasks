#include "wine.h"
#include <iostream>
#include <algorithm>

Wine::Wine() 
     : std::string(""), std::pair<std::valarray<int>, std::valarray<int>>{5, 5} {}

Wine::Wine(const char * label, int year, const int years[], const int bottles[])
     : std::string(label), std::pair<std::valarray<int>, std::valarray<int>>{year, year}
{
    for(int i = 0; i < year; ++i) {
        first[i]  = years[i];
        second[i] = bottles[i];
    }
}

Wine::Wine(const char * label, int year)
     : std::string(label), std::pair<std::valarray<int>, std::valarray<int>>{year, year} {}

void Wine::getBottles()
{
    std::cout << "Enter " << (std::string &) *this << " data for " 
              << first.size() << " year(s):\n";

    for(int i = 0; i < first.size(); ++i) {
        std::cout << "Enter year: ";
        std::cin  >> first[i];
        std::cout << "Enter bottles for that year: ";
        std::cin  >> second[i];
    }
}

std::string Wine::label() const
{
    return (std::string &) *this;
}

void Wine::show() const
{
    std::cout << "Wine: " << (std::string &) *this << '\n';
    std::cout.width(10);
    std::cout << "Year";
    std::cout.width(12);
    std::cout <<"Bottles\n";

    for(int i = 0; i < first.size(); ++i) {
        std::cout.width(10);
        std::cout << first[i];
        std::cout.width(8);
        std::cout << second[i] << '\n';
    }
}

int Wine::sum()
{
    return second.sum();
}

