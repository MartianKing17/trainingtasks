#pragma once
#include "person.h"

class Gunslinger : virtual public Person
{
protected:
    void Data();
public:
    Gunslinger();
    Gunslinger(const Person &other, int number, double time);
    Gunslinger(int number, double time);
    Gunslinger(std::string name, std::string lastname, int number, double time);
    void Show() override;
    double Draw();
private:
    int m_number;
    double m_timeToCombatReadiness;
};
