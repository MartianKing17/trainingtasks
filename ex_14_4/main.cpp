#include "person.h"
#include "gunslinger.h"
#include "pokerplayer.h"
#include "baddude.h"
#include <iostream>
#include <string>
#include <cstring>

using namespace std;

int main()
{
    const int size = 5;
    Person * persons[size]{};
    char choice{};
    int number{}, i{}, count{};
    double time{};
    string name{}, lastname{};

    cout << "Enter the person category:\n"
         << "g: gunslinger, p: pokerplayer, b: baddude, q: quit\n";

    for (i = 0; i < size; ++i) {
        cin >> choice;
	cin.get();
        while(!strchr("gpbq", choice)) {
            cout << "Please enter g, p, b or q: ";
            cin >> choice;
        }

        if (choice == 'q') {
            break;
        }

        cout << "Enter person name: ";
        getline(cin, name);
        cout << "Enter person lastname: ";
        getline(cin, lastname);
        switch(choice) {
            case 'g':
                cout << "Enter number: ";
                cin  >> number;
		        cin.get();
                cout << "Enter time to cambat readiness: ";
                cin  >> time;
		        cin.get();
                persons[i] = new Gunslinger(name, lastname, number, time);
		        break;
            case 'p':
                persons[i] = new PokerPlayer(name, lastname);
		        break;
            case 'b':
                cout << "Enter number: ";
                cin  >> number;
		        cin.get();
                cout << "Enter time to cambat readiness: ";
                cin  >> time;
		        cin.get();
                persons[i] = new BadDude(name, lastname, number, time);
		        break;
        }
    ++count;

	cout << "Enter the person category:\n"
         << "g: gunslinger, p: pokerplayer, b: baddude, q: quit\n";
    }

    cout << "\nHere is your persons:\n";
    for (i = 0; i < count; ++i) {
        persons[i]->Show();
    }
}
