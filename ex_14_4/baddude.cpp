#include "baddude.h"
#include <string>

BadDude::BadDude()
	: Person(), Gunslinger(), PokerPlayer() {}

BadDude::BadDude(int number, double time)
        : Person(), Gunslinger(number, time), PokerPlayer() {}

BadDude::BadDude(std::string name, std::string lastname, int number, double time)
        : Person(name, lastname), Gunslinger(number, time), PokerPlayer() {}


double BadDude::Gdraw()
{
    return Gunslinger::Draw();
}

int BadDude::Cdraw()
{
    return PokerPlayer::Draw();
}

void BadDude::Show()
{
    Gunslinger::Show();
}
