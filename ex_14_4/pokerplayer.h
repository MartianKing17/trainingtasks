#pragma once
#include "person.h"

class PokerPlayer : virtual public Person
{
public:
    PokerPlayer();
    PokerPlayer(std::string name, std::string lastname);
    int Draw();
};
