#include "gunslinger.h"
#include <iostream>

Gunslinger::Gunslinger() : Person(), m_number(0), m_timeToCombatReadiness(0.) {}
Gunslinger::Gunslinger(int number, double time)
           : Person(), m_number(number), m_timeToCombatReadiness(time) {}

Gunslinger::Gunslinger(const Person &other, int number, double time)
	   : Person(other), m_number(number), m_timeToCombatReadiness(time) {}

Gunslinger::Gunslinger(std::string name, std::string lastname, int number, double time)
           : Person(name, lastname), m_number(number), m_timeToCombatReadiness(time) {}
void Gunslinger::Show()
{
    Person::Show();
    Data();
}

void Gunslinger::Data()
{
	std::cout << "Number of: " << m_number 
              << ", time to compat readiness: " << m_timeToCombatReadiness << std::endl;
}

double Gunslinger::Draw()
{
    return m_timeToCombatReadiness;
}

