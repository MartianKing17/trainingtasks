#include "person.h"
#include <iostream>

Person::Person(std::string name, std::string lastname)
       : m_name(name), m_lastname(lastname) {}

Person::Person(const Person &other)
       : m_name(other.m_name), m_lastname(other.m_lastname) {}

void Person::Show()
{
    std::cout << "Fullname: " << m_name << ", " << m_lastname << std::endl;
}
