#pragma once
#include "pokerplayer.h"
#include "gunslinger.h"
#include <string>

class BadDude : public Gunslinger, public PokerPlayer
{
public:
    BadDude();
    BadDude(int number, double time);
    BadDude(std::string name, std::string lastname, int number, double time);
    double Gdraw();
    int Cdraw();
    void Show() override;
};
