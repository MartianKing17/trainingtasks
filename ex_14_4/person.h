#pragma once
#include <string>

class Person
{
public:
    Person(std::string name = "", std::string lastname = "");
    Person(const Person &other);
    virtual void Show();
private:
    std::string m_name;
    std::string m_lastname;
};
