#include <iostream>
#include <string>

using namespace std;

struct CandyBar 
{
  string name;
  double weight;
  int calories;
};

int main()
{
  const int size = 3;
  CandyBar snacks[size]{{"Mocha Munch", 2.3 , 350}, {"Roshen", 0.750, 300}, {"Corona", 0.6, 100}};

  for (const CandyBar snack : snacks)
  {
    cout << "Name: " << snack.name 
       << "\nWeight: " << snack.weight 
       << "\nCalories: " << snack.calories << std::endl;
  }
  
  return 0;
}
