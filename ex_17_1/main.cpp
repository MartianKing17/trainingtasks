#include <iostream>

using namespace std;

int main()
{
    int numberOfChar{};

    while (cin.peek() != '$') {
        ++numberOfChar;
        cin.get();
    }

    cout << "Number of character before $: " << numberOfChar << endl;
    return 0;
}
