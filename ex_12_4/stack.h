
class Stack
{
public:
    Stack(int n = 10);
    Stack(const Stack &st);
    ~Stack();
    bool isEmpty();
    bool isFull();
    bool push(const unsigned int & item);
    bool pop(unsigned int &item);
    Stack &operator=(const Stack &st);
private:
    static const int m_max = 10;
    unsigned int * pItems;
    int m_size;
    int m_top;
};
