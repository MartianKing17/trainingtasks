#include "stack.h"
#include <iostream>
#include <cstring>

Stack::Stack(int n): m_size(0), m_top(0), pItems(nullptr)
{
    if (n <= m_max && n > 0) {
        m_size = n;
    } else if (n <= 0) {
        std::cout << "You enter uncorrect number. Set default number of 10" << std::endl;
        m_size = m_max;
    } else {
        std::cout << "You enter number more 10. Set default number of 10" << std::endl;
        m_size = m_max;
    }
    pItems = new unsigned int[n]{};
}

Stack::Stack(const Stack &st)
{
    m_top = st.m_top;
    m_size = st.m_size;
    pItems = new unsigned int[m_size]{};
    for (int i = 0; i < m_size; ++i) {
        pItems[i] = st.pItems[i];
    }
}

Stack::~Stack()
{
    delete[] pItems;
}

bool Stack::isEmpty()
{
    return m_top == 0;
}

bool Stack::isFull()
{
    return m_top >= m_size;
}

bool Stack::push(const unsigned int & item)
{
    bool res{};
    if (m_top < m_size - 1) {
        pItems[++m_top] = item;
        res = true;
    } 
    
    return res;
}

bool Stack::pop(unsigned int &item)
{   
    bool res{};
    if (m_top > 0) {
        item = pItems[--m_top];
        res = true;
    }

    return res;
}

Stack &Stack::operator=(const Stack &st)
{
    delete[] pItems;
    m_top = st.m_top;
    m_size = st.m_size;
    pItems = new unsigned int[m_size]{};
    for (int i = 0; i < m_size; ++i) {
        pItems[i] = st.pItems[i];
    }

    return *this;
}

        
