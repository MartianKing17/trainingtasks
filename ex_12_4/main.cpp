#include <iostream>
#include "stack.h"

using namespace std;

int main()
{
    unsigned int item = 0;
    Stack stack, newStack{9};
    for (unsigned int i = 0; i < 10; ++i) {
        stack.push(i);
    }

    stack.pop(item);
    newStack = stack;
    newStack.pop(item);
    newStack.pop(item);
    Stack superStack(newStack);
    superStack.push(5);
}
