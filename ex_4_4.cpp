#include <iostream>
#include <string>

using namespace std;

int main()
{
  string firstName;
  string lastName;
  cout << "Enter your name: ";
  getline(cin, firstName);
  cout << "Enter your last name: ";
  getline(cin, lastName);  
  cout << "Here’s the information in a single string: " << lastName 
       << ", " << firstName << std::endl;
  return 0;
}
