#include <iostream>

using namespace std;

int main()
{
    unsigned int degrees{}, minutes{}, seconds{};
    double decimalFormat{};
    const double coefToConvertMinutesToDigrees = 1.666667;
    const double coefToConvertSecondToDegrees = 0.6 * coefToConvertMinutesToDigrees;
    std::cout << "Enter a latitude in degrees, minutes, and seconds:\n";
    std::cout << "First, enter the degrees: ";
    std::cin >> degrees;
    std::cout << "Next, enter the minutes of arc: ";
    std::cin >> minutes;
    std::cout << "Finally, enter the seconds of arc: ";
    std::cin >> seconds;
    decimalFormat = degrees + double(minutes) * coefToConvertMinutesToDigrees
                            + double(seconds) * coefToConvertSecondToDegrees;
    std::cout << degrees << " degrees, " << minutes << " minutes, " << seconds
              << ", seconds = " << decimalFormat << " degrees" << std::endl;
}

